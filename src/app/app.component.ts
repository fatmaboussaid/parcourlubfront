import { Component, OnInit, Input,AfterViewInit } from '@angular/core';

import { AgmMap, MouseEvent, MapsAPILoader } from '@agm/core';
import { TokenStorageService } from './service/token-storage.service';
import { TranslateService } from '@ngx-translate/core';
import { ParametreService } from './service/parametre.service';
import { Parametre } from './model/parametre.model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'TotalEnergies - Ma vidange en ligne - Tunisie';
  cookies: string | null;
  langue: string ;
  classCookies = 'display:block;';
  constructor(public tokenService: TokenStorageService, public translateService: TranslateService, public parametreService: ParametreService) {
    

  }
  acceptCookies() {

    this.tokenService.saveCookies();
    this.getCookies()
  }
  getCookies() {
    this.cookies = this.tokenService.getCookies();
    if (this.cookies) {
      this.classCookies = 'display:none;';
    }
  }
  ngOnInit(): void {
    this.parametreService.getParametre().subscribe((res: Parametre) => {
      //save devise
      if (res.deviseFr && res.deviseEn && res.deviseAr) this.parametreService.saveDevise(res.deviseFr, res.deviseEn, res.deviseAr);
      //save langues  
      if (res.langues) this.parametreService.saveLangues(JSON.stringify(res.langues));
      //get first item in langues 
      if (res.langues.filter(x => typeof x!==undefined).shift()?.toString() !==undefined) {
        this.langue=res.langues.filter(x => typeof x!==undefined).shift()+'';
      }
      this.translateService.addLangs(['fr', 'en', 'ar']);
      //if  langue dosn't exist in localstorge
      if (this.parametreService.getLangue() == null) {
        //if  BrowserLang  exist in langues
        if (this.parametreService.getLangues()?.includes(this.translateService.getBrowserLang())) {
          this.translateService.setDefaultLang(this.translateService.getBrowserLang());
          this.parametreService.saveLangue(this.translateService.getBrowserLang());
          this.translateService.use(this.translateService.getBrowserLang());

          console.log('app1:'+this.translateService.getBrowserLang());
        }
        else{
          this.translateService.setDefaultLang(this.langue);
          this.parametreService.saveLangue(this.langue);
          this.translateService.use(this.langue);

          console.log('app2:'+this.langue);
        }
      }
      else {
        //if  langue  exist in langues
        if (!this.parametreService.getLangues()?.includes(this.parametreService.getLangue()+'')) {
          this.translateService.setDefaultLang(this.langue);
          this. parametreService.saveLangue(this.langue);
          this.translateService.use(this.langue);

          console.log('app3:'+this.langue);
        }
        else{
          this.translateService.setDefaultLang(this.parametreService.getLangue() + '');
          this.translateService.use(this.parametreService.getLangue() + '');

          console.log('app4:'+this.parametreService.getLangue() + '');
        }
      }
    })
    this.getCookies()

  }

  translateSite(langauge: string) {
    this.translateService.use(langauge);
    console.log('app5:'+langauge);

  }

}
