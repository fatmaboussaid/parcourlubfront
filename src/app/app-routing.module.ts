import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotfoundComponent } from './layout/notfound/notfound.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ChoiceLubricantComponent } from './pages/choice-lubricant/choice-lubricant.component';
import { ChoiceLubricant1Component } from './pages/choice-lubricant1/choice-lubricant1.component';
import { ChoiceOfferComponent } from './pages/choice-offer/choice-offer.component';
import { ChoiceStationComponent } from './pages/choice-station/choice-station.component';
import { ChoiceVehiculeComponent } from './pages/choice-vehicule/choice-vehicule.component';
import { DetailsLubricantComponent } from './pages/details-lubricant/details-lubricant.component';
import { DetailsOfferComponent } from './pages/details-offer/details-offer.component';
import { HorairesOfferComponent } from './pages/horaires-offer/horaires-offer.component';
import { HorairesComponent } from './pages/horaires/horaires.component';
import { ListPrestationsComponent } from './pages/list-prestations/list-prestations.component';
import { LubricantComponent } from './pages/lubricant/lubricant.component';
import { MesCommandesComponent } from './pages/mes-commandes/mes-commandes.component';
import { OfferComponent } from './pages/offer/offer.component';
import { SelectVehiculeComponent } from './pages/select-vehicule/select-vehicule.component';
import { ThanksComponent } from './pages/thanks/thanks.component';
import { UpdateHorairesComponent } from './pages/update-horaires/update-horaires.component';
import { UpdatePasswordComponent } from './pages/update-password/update-password.component';
import { UpdateProfileComponent } from './pages/update-profile/update-profile.component';
import { ValidationLubricantComponent } from './pages/validation-lubricant/validation-lubricant.component';
import { ValidationVidangeComponent } from './pages/validation-vidange/validation-vidange.component';
import { ValidationComponent } from './pages/validation/validation.component';

const routes: Routes = [
  {
    path: '',
    component: AccueilComponent
  },
  {
    path: 'lubricants',
    component: LubricantComponent
  },
  {
    path: 'offers',
    component: OfferComponent
  }
  ,
  {
    path: 'detailslubricant',
    component: DetailsLubricantComponent
  },
  {
    path: 'detailsoffer',
    component: DetailsOfferComponent
  },
  {
    path: 'offer',
    component: ChoiceOfferComponent
  },
  {
    path: 'lubricant',
    component: ChoiceLubricantComponent
  },
  {
    path: 'lubricant1',
    component: ChoiceLubricant1Component
  },
  {
    path: 'vehicule',
    component: ChoiceVehiculeComponent
  },
  {
    path: 'station',
    component: ChoiceStationComponent
  },
  {
    path: 'prestations',
    component: ListPrestationsComponent
  },
  {
    path: 'horaires',
    component: HorairesComponent
  },
  {
    path: 'horaires1',
    component: HorairesOfferComponent
  },
  {
    path: 'updatehoraires',
    component: UpdateHorairesComponent
  },
  {
    path: 'validation',
    component: ValidationComponent
  },
  {
    path: 'validation1',
    component: ValidationLubricantComponent
  },
  {
    path: 'validation2',
    component: ValidationVidangeComponent
  },
  {
    path: 'selectvehicule',
    component: SelectVehiculeComponent
  },
  {
    path: 'thanks',
    component: ThanksComponent
  },
  {
    path: 'mescommandes',
    component: MesCommandesComponent
  },
  {
    path: 'reset_password',
    component: UpdatePasswordComponent
  },
  {
    path: 'profil',
    component: UpdateProfileComponent
  },
  {path: '404', component: NotfoundComponent},
  {path: '**', redirectTo: '/404'}

  
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {
   // initialNavigation: 'enabled'
 //  useHash: true
 paramsInheritanceStrategy: 'always',
 initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
