import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/model/user.model';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/service/authentication.service';
import Swal from 'sweetalert2';
import { TokenStorageService } from 'src/app/service/token-storage.service';
export interface RestData {
  email: string;

}
@Component({
  selector: 'app-rest-password',
  templateUrl: './rest-password.component.html',
  styleUrls: ['./rest-password.component.css']
})
export class RestPasswordComponent implements OnInit {
  resetForm = this.formBuilder.group(
    {
      email: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}")]],
    },
    {
      validator: [],
    }
  );
  error: string;
  success: string;
  submitted: boolean = false;
  code: number = 0;
  captchaResponse: string

  constructor(public dialog: MatDialog, public authenticationService: AuthenticationService, private tokenStorageService: TokenStorageService,
    public dialogRef: MatDialogRef<RestPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RestData, private formBuilder: FormBuilder, public modal: NgbActiveModal,) { }

  get form() {
    return this.resetForm?.controls;
  }

  public resolved(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
  onSubmit(): void {
    if (this.captchaResponse) {
      this.error = ''
      this.success = ''
      this.submitted = true;
      let data = this.resetForm.value

      if (this.resetForm.status === 'VALID') {
        this.authenticationService.forgetPassword(data.email).subscribe(
          result => {
            this.code = result.data.code;
            if (this.code == 1) {
              //username is incorrect
              this.error = "Email n'existe pas"
            }
            else if (this.code == 2) {
              //Le compte est désactivé.
              this.error = 'Vous avez déjà demandé un e-mail de réinitialisation de mot de passe. Veuillez vérifier votre email ou réessayer bientôt.'
            }
            else if (this.code == 3) {
              //Le compte est désactivé.
              this.success = 'Un e-mail a été envoyé. Il contient un lien sur lequel il vous faudra cliquer afin de réinitialiser votre mot de passe.'
            }
            else {

            }
          }
        );


      } else if (this.resetForm.status === 'INVALID') {
        this.resetForm.markAllAsTouched()
      }
    } else {
      this.error = 'ReCAPTCHA non valide. Veuillez réessayer.'
    }

  }

  createItem(data: any): FormGroup {
    return this.formBuilder.group(data);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

}
