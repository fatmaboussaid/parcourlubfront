import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Commande } from 'src/app/model/commande.model';
import { Lubricant } from 'src/app/model/lubricant.model';
import { Offer } from 'src/app/model/offer.model';
import { Prestation } from 'src/app/model/prestation.model';
import { Station } from 'src/app/model/station.model';
import { User } from 'src/app/model/user.model';
import { Vehicule } from 'src/app/model/vehicule.model';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
export interface CommandData {
  commande: Commande;
  id: number;
 vehicule: Vehicule;
   station: Station;
   offer: Offer;
   lubricant: Lubricant;
   prestations:Prestation[];
   price: number;
   dateOfReceipt: Date;
   receptionPeriod: string;
   createAt:Date
   orderStatus: number;
   isCompleted: boolean;
   user: User;
   uid: number;
   qrcode: number;
   quantity: number;

}
@Component({
  selector: 'app-details-command',
  templateUrl: './details-command.component.html',
  styleUrls: ['./details-command.component.css']
})
export class DetailsCommandComponent implements OnInit {
  public parametre:Parametre = new Parametre()

  constructor(private parametreServise:ParametreService,
    public dialogRef: MatDialogRef<DetailsCommandComponent>,
    @Inject(MAT_DIALOG_DATA) public commande: CommandData) 
  {
    this.parametre.devise= this.parametreServise.getDevise();
   }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
   if (this.commande.vehicule.marque == undefined)this.commande.vehicule.marque=''
   
  }

}
