import { Component, OnInit,Input,Output } from '@angular/core';
import { Lubricant } from 'src/app/model/lubricant.model';
import { LubricantService } from 'src/app/service/lubricant.service';
import { Observable, throwError } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-lubricant',
  templateUrl: './lubricant.component.html',
  styleUrls: ['./lubricant.component.css']
})
export class LubricantComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  p: number = 1;
  @Input() max = 0;
  public lubricants: Lubricant[] =[]
  constructor(public translateService: TranslateService,private parametreService:ParametreService,public lubricantService:LubricantService, private router: Router) {

   }
  getLuricants() {
    this.lubricantService.getLubricants(this.max).subscribe((res: Lubricant[]) => {
      this.lubricants= res;
    });
  
   
  }
  goToDetails(lubricant:Lubricant){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id":JSON.stringify(lubricant.id),
      }
    };
    this.router.navigate(["/detailslubricant"],navigationExtras);
  
  }
  onImgError(event:any) { 
    event.target.src = 'assets/images/logo_60.png';
  }
  ngOnInit(): void {
    this.parametreService.getParametre().subscribe((res: Parametre) => {
      if (res.deviseFr && res.deviseEn && res.deviseAr){
        var defaultLang=this.translateService.getDefaultLang();
        if(defaultLang == 'fr')this.parametre.devise= res.deviseFr;
        if(defaultLang == 'en')this.parametre.devise= res.deviseEn;
        if(defaultLang == 'ar')this.parametre.devise= res.deviseAr;
      } 
    });
    if(this.parametreService.getDevise())this.parametre.devise= this.parametreService.getDevise();
    this.getLuricants()
  }

}
