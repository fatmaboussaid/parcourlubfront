import { Component, OnInit } from '@angular/core';
import { NavigationEnd,Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { filter } from 'rxjs/operators';
import { Lubricant } from 'src/app/model/lubricant.model';
import { Offer } from 'src/app/model/offer.model';
import { Station } from 'src/app/model/station.model';
import { Vehicule } from 'src/app/model/vehicule.model';
import { Location } from '@angular/common'
import { StationService } from 'src/app/service/station.service';
import { OfferService } from 'src/app/service/offer.service';
import { LubricantService } from 'src/app/service/lubricant.service';
import { Prestation } from 'src/app/model/prestation.model';
import {

  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  ValidatorFn
} from '@angular/forms';
import Swal from 'sweetalert2';

import { Commande } from 'src/app/model/commande.model';
import { CommandeService } from 'src/app/service/commande.service';
import { PrestationStation } from 'src/app/model/prestation-station.model';
import { SearchService } from 'src/app/service/search.service';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-list-prestations',
  templateUrl: './list-prestations.component.html',
  styleUrls: ['./list-prestations.component.css']
})
export class ListPrestationsComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  public vehicule: Vehicule = new Vehicule();
  public station: Station = new Station();
  public offer: Offer;
  public lubricant: Lubricant;
  form: FormGroup;
  prestationsData: PrestationStation[] = [];
  public search: number;

  constructor(private parametreServise:ParametreService,public searchService: SearchService,private formBuilder: FormBuilder, private router: Router, private location: Location, private activatedRoute: ActivatedRoute, public commandeService: CommandeService, public stationService: StationService, public offerService: OfferService, public lubricantService: LubricantService) {
    this.parametre.devise= this.parametreServise.getDevise();
  }


  back(): void {
    this.location.back()
  }
  onCheckboxChange(e: any) {
    const prestations: FormArray = this.form.get('prestations') as FormArray;

    if (e.target.checked) {
      prestations.push(new FormControl(e.target.value));
    } else {
      const index = prestations.controls.findIndex(x => x.value === e.target.value);
      prestations.removeAt(index);
    }
  }

  submit() {


    let commande = new Commande();

   // commande.vehicule = new Vehicule();
    commande.vehicule =this.vehicule;
    commande.station = this.station
    let navigationExtras: NavigationExtras
    if (this.lubricant) commande.lubricant = this.lubricant
    else if (this.offer) commande.offer = this.offer
    
    this.commandeService.saveCommandeStep1(commande, this.form.value.prestations).subscribe((res) => {
      if (res['code'] == 3) {
        this.searchService.steps(this.search,3,'Prestations').subscribe(
          result => {
           
          });
        navigationExtras = {
          queryParams: {
            "id": res['data'],
            "search": this.search,

          }
        };

        this.router.navigate(["/horaires"], navigationExtras);
      }
      else {
        Swal.fire({
          icon: 'error',
          title: 'Erreur Serveur',
          showConfirmButton: false,
          timer: 1500
        })
      }


    });


  }

  goToStations(){
    let navigationExtras: NavigationExtras
    navigationExtras = {
      queryParams: {
        "marque":this.vehicule.marque,
        "modele":this.vehicule.modele,
        "year":this.vehicule.year,
        "carburant":this.vehicule.carburant,
        "search": this.search,

      }
    };

    this.router.navigate(["/vehicule"], navigationExtras);
  }
  onImgError(event: any) {
    event.target.src = 'assets/images/logo_60.png';
  }
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.vehicule.marque = params.marque;
      this.vehicule.modele = params.modele;
      this.vehicule.year = params.year;
      this.vehicule.carburant = params.carburant;
      this.station.id = params.idStation;
      this.search = params.search;
      this.stationService.getStationById(this.station.id).subscribe((res: Station) => {
        this.station = res
        if (params.idOffer) {
          this.offer = new Offer();
          this.offer.id = params.idOffer;
          this.offerService.getOffer(this.offer.id).subscribe((res) => {
            if (res['code'] == 1) {
              this.offer = res['offer'];
              
              for (let i = 0; i < this.station.prestationStations.length; i++) {
                const result = this.offer.prestations.filter(p=>
                   p.id === this.station.prestationStations[i].prestation.id &&
                   p.icon === this.station.prestationStations[i].prestation.icon &&
                   p.name === this.station.prestationStations[i].prestation.name 
                  );
                  if(result.length ==0){
                    this.prestationsData.push(this.station.prestationStations[i])
                  }
              }
             // if (this.offer.quantity == 0) this.router.navigate(["/"]);
            }
            else this.router.navigate(["/"]);
  
          });
  
  
          
        }
        else if (params.idLubricant) {
          this.lubricant = new Lubricant();
          this.lubricant.id = params.idLubricant;
  
          this.lubricantService.getLubricant(this.lubricant.id).subscribe((res) => {
            if (res['code'] == 1) {
              this.lubricant = res['product'];
             // if (this.lubricant.quantity == 0) this.router.navigate(["/"]);
            }
            else this.router.navigate(["/"]);
          });
          this.prestationsData = this.station.prestationStations
  
        }

       
      //  this.prestationsData = this.station.prestationStations.filter(prestationStation => prestationStation.prestation.(filterValue));
        this.form = this.formBuilder.group({
          prestations: this.formBuilder.array([])
        })
      });


    });


  }

}
