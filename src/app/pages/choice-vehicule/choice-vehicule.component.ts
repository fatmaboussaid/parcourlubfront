import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Station } from 'src/app/model/station.model';
import { Vehicule } from 'src/app/model/vehicule.model';
import { OfferService } from 'src/app/service/offer.service';
import { SearchService } from 'src/app/service/search.service';
import { StationService } from 'src/app/service/station.service';
import { UtilsService } from 'src/app/service/utils.service';

@Component({
  selector: 'app-choice-vehicule',
  templateUrl: './choice-vehicule.component.html',
  styleUrls: ['./choice-vehicule.component.css']
})
export class ChoiceVehiculeComponent implements OnInit {
  public latitude: number = 0;
  public longitude: number = 0;
  public typedevice: number;
  public search: number;
  public vehicule: Vehicule = new Vehicule();
  public stations: Station[] = []
  public isOilChange = true;
  constructor( public searchService: SearchService,private activatedRoute: ActivatedRoute, public utilsService: UtilsService, public stationService: StationService, public offerService: OfferService, private router: Router) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.vehicule.marque = params.marque;
      this.vehicule.modele = params.modele;
      this.vehicule.year = params.year;
      this.vehicule.carburant = params.carburant;
      this.latitude = params.latitude;
      this.longitude = params.longitude;
      this.search = params.search;

    });

    this.stationService.getStations(this.isOilChange, this.latitude, this.longitude).subscribe((res: Station[]) => {
      this.stations = res
    });



  }
  getLocation() {
    if (navigator.geolocation) {
      var location_timeout = setTimeout("geolocFail()", 10000);
      navigator.geolocation.getCurrentPosition(position => {
        clearTimeout(location_timeout);
        if (position) {
          this.latitude = position.coords.latitude
          this.longitude = position.coords.longitude
        }
      },
        function (error) {

        }, {
        enableHighAccuracy: true
        , timeout: 5000
      }
      );

    } else {
    }

  }
  getTypeDevice() {
    //this.typedevice = this.utilsService.getTypeDevice();
  }
  goToStation(stationId: number) {

    this.searchService.steps(this.search,1,'Station').subscribe(
      result => {
       
      });
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "marque": this.vehicule.marque,
        "modele": this.vehicule.modele,
        "year": this.vehicule.year,
        "carburant": this.vehicule.carburant,
        "id": stationId,
        "search": this.search,

      }
    };
    this.router.navigate(["/station"], navigationExtras);
  }
  ngOnInit(): void {

  }

}
