import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceVehiculeComponent } from './choice-vehicule.component';

describe('ChoiceVehiculeComponent', () => {
  let component: ChoiceVehiculeComponent;
  let fixture: ComponentFixture<ChoiceVehiculeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoiceVehiculeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiceVehiculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
