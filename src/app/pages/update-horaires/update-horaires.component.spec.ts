import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateHorairesComponent } from './update-horaires.component';

describe('UpdateHorairesComponent', () => {
  let component: UpdateHorairesComponent;
  let fixture: ComponentFixture<UpdateHorairesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateHorairesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateHorairesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
