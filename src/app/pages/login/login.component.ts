import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/model/user.model';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/service/authentication.service';
import Swal from 'sweetalert2';
import { RegisterComponent } from '../register/register.component';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { RestPasswordComponent } from 'src/app/modal/rest-password/rest-password.component';
export interface LoginData {
  email: string;
  password: string;

}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = this.formBuilder.group(
    {
      email: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}")]],
      password: ['', Validators.required],
    },
    {
      validator: [],
    }
  );
  error: string;
  submitted: boolean = false;
  code: number = 0;
  captchaResponse: string
  constructor(public dialog: MatDialog, public authenticationService: AuthenticationService, private tokenStorageService: TokenStorageService,
    public dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData, private formBuilder: FormBuilder, public modal: NgbActiveModal,) { }



  get form() {
    return this.loginForm?.controls;
  }


  onSubmit(): void {
    if (this.captchaResponse) {
      this.error = ''
      this.submitted = true;
      let data = this.loginForm.value

      if (this.loginForm.status === 'VALID') {
        this.authenticationService.generateToken(data.email, data.password).subscribe(
          result => {
            this.code = result.data.code;
            if (this.code == 1) {
              //username is incorrect
              this.error = 'Email ou mot de passe incorrect'
            }
            else if (this.code == 2) {
              //Le compte est désactivé.
              this.error = 'Email ou mot de passe incorrect'
            }
            else if (this.code == 3) {
              //Le compte est supprimer.
              this.error = 'Email ou mot de passe incorrect'
            }
            else if (this.code == 4) {
              //password is incorrect
              this.error = 'Email ou mot de passe incorrect'
            }
            else if (this.code == 5) {

              this.authenticationService.login(result.data.token).subscribe(
                result1 => {
                  this.code = result1.data.code;
                  if (this.code == 1) {
                    //invalid token
                    this.error = 'Email ou mot de passe incorrect'
                  }
                  else if (this.code == 2) {
                    //email incorrect 
                    this.error = 'Email ou mot de passe incorrect'

                  }
                  else if (this.code == 3) {
                    //compte deactive 
                    this.error = 'Email ou mot de passe incorrect'

                  }
                  else if (this.code == 4) {
                    //incorrect password
                    this.error = 'Email ou mot de passe incorrect'

                  }
                  else if (this.code == 5) {
                    this.tokenStorageService.saveToken(result.data.token)
                    this.tokenStorageService.saveUser(result1.data.user)
                    window.location.reload();

                  }
                })
            }
            else {

            }
          }
        );


        /*
        this.authenticationService.register(user).subscribe(
          result => {
            this.code = result.data.code
            if(this.code ==1){
              Swal.fire({
                icon: 'error',
                title: 'Erreur Serveur',
                showConfirmButton: false,
                timer: 1500
              })
            }
            else if(this.code== 2){
              this.emailUsed=data.email
            }
            else if(this.code== 3){
              Swal.fire({
                icon: 'success',
                title: 'Votre compte a été créer avec success',
                showConfirmButton: false,
                timer: 1500
              })
              this.addUserForm.reset();
              this.submitted = false;
              this.onNoClick()
  
            }
            else{
  
            }
  
           // 
            
            //message success
          },
          error => {
                        //error serveur
          }
        );*/
      } else if (this.loginForm.status === 'INVALID') {
        this.loginForm.markAllAsTouched()
      }
    }
    else {
      this.error = 'ReCAPTCHA non valide. Veuillez réessayer.'
    }

  }
  openDialogResetPassword() {
    this.onNoClick()
    const dialogRef = this.dialog.open(RestPasswordComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
  createItem(data: any): FormGroup {
    return this.formBuilder.group(data);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  openDialogRegister(): void {
    this.onNoClick()
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
  public resolved(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
  ngOnInit(): void {
  }

}
