import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HorairesOfferComponent } from './horaires-offer.component';

describe('HorairesOfferComponent', () => {
  let component: HorairesOfferComponent;
  let fixture: ComponentFixture<HorairesOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HorairesOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HorairesOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
