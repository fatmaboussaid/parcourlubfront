import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common'
import { CommandeService } from 'src/app/service/commande.service';
import { Commande } from 'src/app/model/commande.model';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';

import {

  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  ValidatorFn
} from '@angular/forms';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
registerLocaleData(localeFr, 'fr');
@Component({
  selector: 'app-horaires-offer',
  templateUrl: './horaires-offer.component.html',
  styleUrls: ['./horaires-offer.component.css']
})
export class HorairesOfferComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  datepipe: DatePipe = new DatePipe('fr-FR')

  selectedDate: Date|null  = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);;
  selectedHour: string;

  minDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
  horaireValues: { [key: number]: Date } 
  dates: Date[] = []
  commandesByDate:Commande[]=[]
  public commande: Commande = new Commande();
  constructor(private parametreServise:ParametreService,private _adapter: DateAdapter<any>,public datePipe: DatePipe,private formBuilder: FormBuilder, private router: Router, private location: Location, 
    private activatedRoute: ActivatedRoute, public commandeService: CommandeService) {
      this.parametre.devise= this.parametreServise.getDevise();
      this._adapter.setLocale('fr');
  }
  onImgError(event: any) {
    event.target.src = 'assets/images/logo_60.png';
  }
  getHoraire(key: number): Date {
    return this.horaireValues[key];
  }
  sundaysDatesFilter = (d: Date): boolean => {
    const day = d.getDay();
    /* Prevent Saturday and Sunday for select. */
    if (this.commande.station.isClosedSunday == true)
      return day !== 0;
    else return true
  }

  getDates(date: Date): Date[] {
    let dates: Date[] = []
    dates.push(date)
    dates.push(this.addMinutes(date, 30))
    dates.push(this.addMinutes(date, 60))
    dates.push(this.addMinutes(date, 90))
    return dates;
  }
  addMinutes(date: Date, minutes: number) {
    return new Date(date.getTime() + minutes * 60000);
  }
  submit(){
    let navigationExtras: NavigationExtras

    this.commandeService.saveCommandeStep2(this.commande.id,this.datepipe.transform(this.selectedDate, 'd-MM-Y')+'',this.selectedHour).subscribe((res) => {
      if (res['code'] ==2) {
        navigationExtras = {
          queryParams: {
            "id": res['data'],
          }
        };

        this.router.navigate(["/validation2"], navigationExtras);
      }
      else {
        Swal.fire({
          icon: 'error',
          title: 'Erreur Serveur',
          showConfirmButton: false,
          timer: 1500
        })
      }


    });
  }

  back(): void {
    this.location.back()
  }
  goToStations(){
    let navigationExtras: NavigationExtras
    if(this.commande.offer)  {

      navigationExtras = {
        queryParams: {
          "id":this.commande.offer.id,
        }
      };
      this.router.navigate(["/offer"], navigationExtras);
    } 
    else if(this.commande.lubricant)   {
      navigationExtras = {
        queryParams: {
          "id":this.commande.lubricant.id,
          "quantity":this.commande.quantity
        }
      };
      this.router.navigate(["/lubricant1"], navigationExtras);
    }


 
  }

  updateDate(event:any){
    this.selectedDate=event
    this.addHoraireValues(event);
    this.commande.station.horaires.forEach((horaire, index) => {
      let date = this.getHoraire(horaire);
      this.addHoraires(date);
    });
    
  }
  addHoraires(date:any){
    this.dates=[]
    this.commandeService.getCommandesByDate(this.datePipe.transform(date, 'dd/MM/yyyy')).subscribe((commandesByDate) => {
      this.commandesByDate=commandesByDate
      this.getDates(date).forEach((date, index) => {
        let hour= this.datePipe.transform(date, 'HH:mm');
        const result = this.commandesByDate.filter(c=>
          c.receptionPeriod === hour
         );
         if(result.length ==0){
          this.dates.push(date);
         }
      })
    
    });
  }
  addHoraireValues(date:Date){
    this.horaireValues={
      1: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 8, 0, 0),
      2: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 10, 0, 0),
      3: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 12, 0, 0),
      4: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 14, 0, 0),
      5: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 16, 0, 0),
      6: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 18, 0, 0)
    };
  }
  ngOnInit(): void {
   this.addHoraireValues(this.minDate);
    this.activatedRoute.queryParams.subscribe(params => {

      if (params.id) {
        this.commandeService.getCommande(params.id).subscribe((res) => {
          this.commande = res
          this.commande.station.horaires.sort((a,b) =>   a-b );
          this.commande.station.horaires.forEach((horaire, index) => {

            let date = this.getHoraire(horaire);
            
            this.addHoraires(date);
          });

          

        });
      }
      else {
        this.router.navigate(["/"]);

      }

    });
  }

}

