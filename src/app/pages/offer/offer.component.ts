import { Component, OnInit,Input } from '@angular/core';
import { Offer } from 'src/app/model/offer.model';
import { OfferService } from 'src/app/service/offer.service';
import { Router, NavigationExtras } from '@angular/router';
import { EventEmitterService } from 'src/app/service/event-emitter.service';
import { BehaviorSubject } from 'rxjs';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  @Input() max = 0;
  @Input() search = "";
  p: number = 1;
  verif: boolean = true;
  defaultLang:string='';
  //public offersFiltered$ = new   BehaviorSubject<Offer[] | null>(null);

  public offers: Offer[] =[]
 public offersFiltered$: Offer[] =[]
  constructor(public translateService: TranslateService,private parametreService:ParametreService,   private eventEmitterService: EventEmitterService ,public offerService: OfferService, private router: Router) { 
    this.parametreService.getParametre().subscribe((res: Parametre) => {
      if (res.deviseFr && res.deviseEn && res.deviseAr){
        this.defaultLang=this.translateService.getDefaultLang();
        if(this.defaultLang == 'fr')this.parametre.devise= res.deviseFr;
        if(this.defaultLang == 'en')this.parametre.devise= res.deviseEn;
        if(this.defaultLang == 'ar')this.parametre.devise= res.deviseAr;
      } 
  
    });
    if(this.parametreService.getDevise())this.parametre.devise= this.parametreService.getDevise();
  }
  getOffers(){
    this.offerService.getOffers(this.max).subscribe((res: Offer[]) => {
      this.offers= res;
      this.offersFiltered$= res;
     // this.offersFiltered$.next(res); 

    });
  }
  goToSelectStation(offer:Offer){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id":JSON.stringify(offer.id),
      }
    };
    this.router.navigate(["/offer"],navigationExtras);
  
  }
  goToDetails(offer:Offer){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id":JSON.stringify(offer.id),
      }
    };
    this.router.navigate(["/detailsoffer"],navigationExtras);
  
  }
  filterOffers(search:string){
    this.p=1
    this.offersFiltered$=[]
    this.offersFiltered$ = this.offers?.filter(offer => { 
      console.log(search);
      return  offer.name.toUpperCase().includes(search.toUpperCase());
   });
  }
  ngOnInit(): void {
    this.getOffers();
    if (this.eventEmitterService.subsVar==undefined) {    
      this.eventEmitterService.subsVar = this.eventEmitterService.    
      search.subscribe((search:string) => {    
        this.filterOffers(search);    
      });    
    }  
    
  }

}
