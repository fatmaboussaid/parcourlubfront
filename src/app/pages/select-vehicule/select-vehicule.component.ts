import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";

import { Lubricant } from 'src/app/model/lubricant.model';
import { Offer } from 'src/app/model/offer.model';
import { Station } from 'src/app/model/station.model';
import { Vehicule } from 'src/app/model/vehicule.model';
import { Location } from '@angular/common'
import { StationService } from 'src/app/service/station.service';
import { OfferService } from 'src/app/service/offer.service';
import { LubricantService } from 'src/app/service/lubricant.service';
import { Prestation } from 'src/app/model/prestation.model';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  ValidatorFn
} from '@angular/forms';
import Swal from 'sweetalert2';

import { Commande } from 'src/app/model/commande.model';
import { CommandeService } from 'src/app/service/commande.service';
import { Marque } from 'src/app/model/marque.model';
import { Modele } from 'src/app/model/modele.model';
import { Observable } from 'rxjs';
import { MarqueService } from 'src/app/service/marque.service';
import { SearchService } from 'src/app/service/search.service';
import { map, startWith } from 'rxjs/operators';
import { PrestationStation } from 'src/app/model/prestation-station.model';
import {listCarburants} from "src/app/service/const"

@Component({
  selector: 'app-select-vehicule',
  templateUrl: './select-vehicule.component.html',
  styleUrls: ['./select-vehicule.component.css']
})
export class SelectVehiculeComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  public vehicule: Vehicule = new Vehicule();
  public station: Station = new Station();
  public offer: Offer;
  public lubricant: Lubricant;
  form: FormGroup;
  prestationsData: PrestationStation[] = [];
  vehiculeSelected = false;


  public marques: Marque[] = []
  public modeles: Modele[] = []
  years: string[];
  carburants: string[];
  filteredMarques: Observable<Marque[]>;
  filteredModeles: Observable<Modele[]>;
  filteredYears: Observable<string[]>;
  filteredCarburants: Observable<string[]>;
  marqueSelected: Marque
  modeleSelected: Modele
  yearSelected: string = ''
  carburantSelected: string = ''
  marque = new FormControl('', Validators.required)
  modele = new FormControl('', Validators.required)
  year = new FormControl('', Validators.required)
  carburant = new FormControl('', Validators.required)
  submitted: boolean = false
  quantity: number=1

  public formSearch = this.formBuilder.group(
    {
      marque: this.marque,
      modele: this.modele,
      year: this.year,
      carburant: this.carburant,
    },
    {
      validator: [],
    }
  );
  constructor(private parametreServise:ParametreService,public marqueService: MarqueService, public searchService: SearchService, private formBuilder: FormBuilder, private router: Router, private location: Location, private activatedRoute: ActivatedRoute, public commandeService: CommandeService, public stationService: StationService, public offerService: OfferService, public lubricantService: LubricantService) {
    this.parametre.devise= this.parametreServise.getDevise();
    this.formSearch = this.formBuilder.group(
      {
        marque: this.marque,
        modele: this.modele,
        year: this.year,
        carburant: this.carburant,
      },
      {
        validator: [],
      }
    );

    this.activatedRoute.queryParams.subscribe(params => {
      this.station.id = params.idStation;
      this.stationService.getStationById(this.station.id).subscribe((res: Station) => {
        this.station = res
        if (params.idOffer) {
          this.offer = new Offer();
          this.offer.id = params.idOffer;
          if(params.quantity)this.quantity = params.quantity;
          this.offerService.getOffer(this.offer.id).subscribe((res) => {
            if (res['code'] == 1) {
              this.offer = res['offer'];
              for (let i = 0; i < this.station.prestationStations.length; i++) {
                const result = this.offer.prestations.filter(p =>
                  p.id === this.station.prestationStations[i].prestation.id &&
                  p.icon === this.station.prestationStations[i].prestation.icon &&
                  p.name === this.station.prestationStations[i].prestation.name
                );
                if (result.length == 0) {
                  this.prestationsData.push(this.station.prestationStations[i])
                }
              }
              //  if (this.offer.quantity == 0) this.router.navigate(["/"]);
            }
            else this.router.navigate(["/"]);

          });
        }
        else if (params.idLubricant) {
          this.lubricant = new Lubricant();
          this.lubricant.id = params.idLubricant;
          this.quantity = params.quantity;
          this.lubricantService.getLubricant(this.lubricant.id).subscribe((res) => {
            if (res['code'] == 1) {
              this.lubricant = res['product'];
              this.prestationsData = this.station.prestationStations

              // if (this.lubricant.quantity == 0) this.router.navigate(["/"]);
            }
            else this.router.navigate(["/"]);
          });

        }






        this.form = this.formBuilder.group({
          prestations: this.formBuilder.array([])
        })
      });
    });

    this.marqueService.getMarque().subscribe((res: Marque[]) => {
      this.marques = res;
      let autreMarque: Marque = new Marque();
      autreMarque.id = 0;
      autreMarque.marque = 'Autre';
      this.marques.push(autreMarque);
      this.filteredMarques = this.marque.valueChanges
        .pipe(
          startWith(''),
          map(state => state ? this._filterMarque(state) : this.marques.slice())
        );
    });

  }
  onCheckboxChange(e: any) {
    const prestations: FormArray = this.form.get('prestations') as FormArray;

    if (e.target.checked) {
      prestations.push(new FormControl(e.target.value));
    } else {
      const index = prestations.controls.findIndex(x => x.value === e.target.value);
      prestations.removeAt(index);
    }
  }

  onImgError(event: any) {
    event.target.src = 'assets/images/logo_60.png';
  }
  back(): void {
    this.location.back()
  }

  getMarques() {
    this.marqueService.getMarque().subscribe((res: Marque[]) => {
      this.marques = res;
    });


  }
  onSelectMarque(marque: Marque) {
    this.modele.setValue('');
    this.year.setValue('');
    this.carburant.setValue('');
    this.carburantSelected = ''
    if(marque.id==0){
      let autreModele= new Modele()
      autreModele.id=0
      autreModele.modele='Autre'
      autreModele.marque=marque
      this.modeles.push(autreModele);
    }
    else{
    this.modeles = marque.modeles;
    }
    this.filteredModeles = this.modele.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.modele),
      map(modele => modele ? this._filterModele(modele) : this.modeles.slice())
    );

  }
  onSelectModele(modele: Modele) {
    this.year.setValue('');
    this.carburant.setValue('');
    this.carburantSelected = ''
    this.years = []
    if(modele.id==0){
      this.years.push('Autre');
    }
    else{
      if ((modele.annee_debut != '') && (modele.annee_fin != '')) {

        for (let i = Number(modele.annee_debut); i <= Number(modele.annee_fin); i++) {
          this.years.push(String(i));
        }
  
      }
      else if ((modele.annee_debut != '') && (modele.annee_fin == '')) this.years.push(modele.annee_debut);
      else if ((modele.annee_debut == '') && (modele.annee_fin != '')) this.years.push(modele.annee_fin);
    }

    this.filteredYears = this.year.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(year => year ? this._filterYear(year) : this.years.slice())
    );

  }
  onSelectYear(year: string) {
    this.yearSelected = year
    this.carburantSelected = ''
    this.carburant.setValue('');

    this.carburants = listCarburants
    this.filteredCarburants = this.carburant.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(carburant => carburant ? this._filterCarburant(carburant) : this.carburants.slice())
    );
  }
  onSelectCarburant(carburant: string) {
    this.carburantSelected = carburant
    this.vehiculeSelected = true
    this.vehicule = this.formSearch.value

  }
  submit() {

    let commande = new Commande();

    // commande.vehicule = new Vehicule();
    commande.vehicule = this.vehicule;
    commande.station = this.station
    let navigationExtras: NavigationExtras
    if (this.lubricant) {
      commande.lubricant = this.lubricant
      commande.quantity = this.quantity
    }
    else if (this.offer) {
      commande.offer = this.offer
      commande.quantity = this.quantity
    }
    this.commandeService.saveCommandeStep1(commande, this.form.value.prestations).subscribe((res) => {
      if (res['code'] == 3) {
        navigationExtras = {
          queryParams: {
            "id": res['data'],
          }
        };

        this.router.navigate(["horaires1"], navigationExtras);
      }
      else {
        Swal.fire({
          icon: 'error',
          title: 'Erreur Serveur',
          showConfirmButton: false,
          timer: 1500
        })
      }
    })
  }
  ngOnInit(): void {
    this.getMarques();
    this.filteredMarques = this.marque.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.marque),
      map(marque => marque ? this._filterMarque(marque) : this.marques.slice())
    );
  }

  private _filterMarque(value: string): Marque[] {
    const filterValue = value.toLowerCase();
    return this.marques.filter(marque => marque.marque.toLowerCase().includes(filterValue));
  }
  displayMarque(marque: string): string {
    return marque && marque ? marque : '';
  }



  private _filterModele(value: string): Modele[] {
    const filterValue = value.toLowerCase();
    return this.modeles.filter(modele => modele.modele.toLowerCase().includes(filterValue));
  }
  displayModele(modele: string): string {
    return modele && modele ? modele : '';
  }


  private _filterYear(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.years.filter(year => year.toLowerCase().includes(filterValue));
  }
  displayYear(year: string): string {
    return year ? year : '';
  }

  private _filterCarburant(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.carburants.filter(carburant => carburant.toLowerCase().includes(filterValue));
  }
  displayCarburant(carburant: string): string {
    return carburant ? carburant : '';
  }

}
