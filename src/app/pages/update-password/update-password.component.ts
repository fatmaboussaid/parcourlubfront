import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {
token:string
pwdsDontMatch=false
submitted: boolean = false;
code: number = 0;

error:string;
success:string;
resetPasswordForm = this.formBuilder.group(
  {

    password: ['', [Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}")]],
    confirmpassword: ['', [Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}")]],

  }, {
    validator:  this.matchPwds
  });

  constructor(public authenticationService: AuthenticationService, private activatedRoute: ActivatedRoute,private router: Router,private formBuilder: FormBuilder) { }
  get form() {
    return this.resetPasswordForm?.controls;
  }

  matchPwds(control: AbstractControl) {
    let password = control.get('password')?.value;
    let confirmpassword = control.get('confirmpassword')?.value;
    
    if(password !== confirmpassword){
      
      return { pwdsDontMatch: true };
    }
    return null;
  }
  onSubmit(){
    this.submitted = true;
    this.error=''
    this.success=''
    let data = this.resetPasswordForm.value





    if (this.resetPasswordForm.status === 'VALID') {
      this.authenticationService.resetPassword(this.token,data.password,data.confirmpassword).subscribe(
        result => {
          this.code = result.data.code
          if (this.code == 1) {
            //username is incorrect
            this.error = "Le lien de réinitialisation du mot de passe n'est pas valide. Veuillez réessayer de réinitialiser votre mot de passe."
          }
          else if (this.code == 2) {
            //Le compte est désactivé.
            this.error = "Aucun token de réinitialisation de mot de passe trouvé dans l'URL"
          }
          else if (this.code == 3) {
            //Le compte est désactivé.
            this.success = 'Votre mot de passe est modifier avec success'
          }
          else {

          }

          // 

          //message success
        },
        error => {
          //error serveur
        }
      );
    } else if (this.resetPasswordForm.status === 'INVALID') {
      this.resetPasswordForm.markAllAsTouched()
    }
  }
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {

      if (params.token) {
        this.token=params.token
       
      }
      else {
        this.router.navigate(["/"]);
      }

    });
  }

}
