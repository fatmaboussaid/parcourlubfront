import { Component, OnInit } from '@angular/core';
import {Router, NavigationExtras,ActivatedRoute } from "@angular/router";
import { Lubricant } from 'src/app/model/lubricant.model';
import { LubricantService } from 'src/app/service/lubricant.service';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-details-lubricant',
  templateUrl: './details-lubricant.component.html',
  styleUrls: ['./details-lubricant.component.css']
})
export class DetailsLubricantComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  public lubricant: Lubricant;
  public quantity:number=1;
  public id:number;
  constructor(private parametreServise:ParametreService,private activatedRoute: ActivatedRoute,public lubricantService:LubricantService, private router: Router) { 
    this.parametre.devise= this.parametreServise.getDevise();
    this.activatedRoute.queryParams.subscribe(params => {
      this.id=params["id"];
      this.lubricantService.getLubricant(this.id).subscribe((res) => {
         if(res['code'] == 1){
           this.lubricant=res['product'];
         //  if(this.lubricant.quantity>0) this.quantity =1;
        //   else this.quantity =0;
          }
         else this.router.navigate(["/"]);

      });
    });
  }
  onClickMinus(){
    if(this.quantity>1) this.quantity --;
  }
  onClickPlus(){
   // if(this.quantity<this.lubricant.quantity) 
    this.quantity ++;
  }
  onImgError(event:any) { 
    event.target.src = 'assets/images/logo_60.png';
  }
  goToStations(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id":this.lubricant.id,
        "quantity":this.quantity
      }
    };
    this.router.navigate(["/lubricant"],navigationExtras);
  }
  goToSelectStation(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id":this.lubricant.id,
        "quantity":this.quantity

      }
    };
    this.router.navigate(["/lubricant1"],navigationExtras);
  
  }
  ngOnInit(): void {
    
  }

}
