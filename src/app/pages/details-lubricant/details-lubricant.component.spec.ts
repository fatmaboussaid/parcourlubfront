import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsLubricantComponent } from './details-lubricant.component';

describe('DetailsLubricantComponent', () => {
  let component: DetailsLubricantComponent;
  let fixture: ComponentFixture<DetailsLubricantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsLubricantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsLubricantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
