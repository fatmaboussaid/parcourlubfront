import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry } from 'rxjs/operators';


import { Marque } from 'src/app/model/marque.model';
import { MarqueService } from 'src/app/service/marque.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { catchError, map, startWith } from 'rxjs/operators';
import { Modele } from 'src/app/model/modele.model';
import { SearchService } from 'src/app/service/search.service';
import { Router, NavigationExtras } from '@angular/router';
import { listCarburants } from "src/app/service/const"
import { UtilsService } from 'src/app/service/utils.service';
import { ParametreService } from 'src/app/service/parametre.service';
import { Parametre } from 'src/app/model/parametre.model';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  // maps
  latitude: number;
  longitude: number;
  public address: string='';
  typedevice: number;




  public marques: Marque[] = []
  public modeles: Modele[] = []
  years: string[];
  carburants: string[];
  filteredMarques: Observable<Marque[]>;
  filteredModeles: Observable<Modele[]>;
  filteredYears: Observable<string[]>;
  filteredCarburants: Observable<string[]>;
  marqueSelected: Marque
  modeleSelected: Modele
  yearSelected: string = ''
  carburantSelected: string = ''
  marque = new FormControl('', Validators.required)
  modele = new FormControl('', Validators.required)
  year = new FormControl('', Validators.required)
  carburant = new FormControl('', Validators.required)
  submitted: boolean = false;

  formSearch = this.formBuilder.group(
    {
      marque: this.marque,
      modele: this.modele,
      year: this.year,
      carburant: this.carburant,
    },
    {
      validator: [],
    }
  );

  constructor(private parametreServise:ParametreService, private utilsService: UtilsService, public marqueService: MarqueService, private formBuilder: FormBuilder, public searchService: SearchService, private router: Router) {
  
    
    this.marqueService.getMarque().subscribe((res: Marque[]) => {
      this.marques = res;
      let autreMarque: Marque = new Marque();
      autreMarque.id = 0;
      autreMarque.marque = 'Autre';
      this.marques.push(autreMarque);
      this.filteredMarques = this.marque.valueChanges
        .pipe(
          startWith(''),
          map(state => state ? this._filterMarque(state) : this.marques.slice())
        );
    });








  }

  getMarques() {


  }
  onSelectMarque(marque: Marque) {
    this.modele.setValue('');
    this.year.setValue('');
    this.carburant.setValue('');
    this.carburantSelected = ''
    if (marque.id == 0) {
      let autreModele = new Modele()
      autreModele.id = 0
      autreModele.modele = 'Autre'
      autreModele.marque = marque
      this.modeles.push(autreModele);
    }
    else {
      this.modeles = marque.modeles;
    }

    this.filteredModeles = this.modele.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.modele),
      map(modele => modele ? this._filterModele(modele) : this.modeles.slice())
    );

  }
  onSelectModele(modele: Modele) {
    this.year.setValue('');
    this.carburant.setValue('');
    this.carburantSelected = ''
    this.years = []
    if (modele.id == 0) {
      this.years.push('Autre');
    }
    else {
      if ((modele.annee_debut != '') && (modele.annee_fin != '')) {

        for (let i = Number(modele.annee_debut); i <= Number(modele.annee_fin); i++) {
          this.years.push(String(i));
        }

      }
      else if ((modele.annee_debut != '') && (modele.annee_fin == '')) this.years.push(modele.annee_debut);
      else if ((modele.annee_debut == '') && (modele.annee_fin != '')) this.years.push(modele.annee_fin);

    }

    this.filteredYears = this.year.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(year => year ? this._filterYear(year) : this.years.slice())
    );


  }
  onSelectYear(year: string) {
    this.yearSelected = year
    this.carburantSelected = ''
    this.carburant.setValue('');

    this.carburants = listCarburants
    this.filteredCarburants = this.carburant.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(carburant => carburant ? this._filterCarburant(carburant) : this.carburants.slice())
    );
  }

  onSelectCarburant(carburant: string) {


    this.carburantSelected = carburant
  }
  search() {
    this.searchService.saveSearch(this.formSearch.value,this.address).subscribe(
      result => {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            "marque": this.formSearch.value.marque,
            "modele": this.formSearch.value.modele,
            "year": this.formSearch.value.year,
            "carburant": this.formSearch.value.carburant,
            "latitude": this.latitude,
            "longitude": this.longitude,
            "search": result,

          }
        };
        this.router.navigate(["/vehicule"], navigationExtras);

       
      });



  }
  
  getLocation() {
    if (navigator.geolocation) {
      var location_timeout = setTimeout("geolocFail()", 10000);
      navigator.geolocation.getCurrentPosition(position => {
        clearTimeout(location_timeout);
        if (position) {
          this.latitude = position.coords.latitude
          this.longitude = position.coords.longitude
          this.getAddress(this.latitude, this.longitude);
        }
      },
        function (error) {

        }, {
        enableHighAccuracy: true
        , timeout: 5000
      }
      );

    } else {
    }

  }

  getTypeDevice() {
    this.typedevice = this.utilsService.getTypeDevice();

  }
  getAddress(laltitude: number, longitude: number) {
    this.utilsService.getAddress(laltitude, longitude).subscribe((result) => {
      if (result['status']=="OK") {
        this.address = result['results']['0']['formatted_address'];
        
      }
    });
  }
  ngOnInit(): void {
    this.getTypeDevice();
    
    if(this.typedevice == 1 || this.typedevice == 2){
       let result = this.getLocation();
    }
  }






  private _filterMarque(value: string): Marque[] {
    const filterValue = value.toLowerCase();
    return this.marques.filter(marque => marque.marque.toLowerCase().includes(filterValue));
  }
  displayMarque(marque: string): string {
    return marque && marque ? marque : '';
  }



  private _filterModele(value: string): Modele[] {
    const filterValue = value.toLowerCase();
    return this.modeles.filter(modele => modele.modele.toLowerCase().includes(filterValue));
  }
  displayModele(modele: string): string {
    return modele && modele ? modele : '';
  }


  private _filterYear(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.years.filter(year => year.toLowerCase().includes(filterValue));
  }
  displayYear(year: string): string {
    return year ? year : '';
  }

  private _filterCarburant(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.carburants.filter(carburant => carburant.toLowerCase().includes(filterValue));
  }
  displayCarburant(carburant: string): string {
    return carburant ? carburant : '';
  }

}
