
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/model/user.model';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/service/authentication.service';
import Swal from 'sweetalert2';
import { TokenStorageService } from 'src/app/service/token-storage.service';
//import { LoginComponent } from '../login/login.component';
export interface RegisterData {
  user: User;

}
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  addUserForm = this.formBuilder.group(
    {
      firstName: ['', Validators.required],
      //lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}")]],
      password: ['', [Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}")]],
      tel: ['', [Validators.required, Validators.pattern('^[0-9]{8}')]],
      acceptSms: ['', []],
      acceptEmail: ['', []],

    },
    {
      validator: [],
    }
  );
  submitted: boolean = false;
  user: User = new User();
  code: number = 0;
  emailUsed: string;
  captchaResponse: string
  acceptSms:boolean;
  accepEmail:boolean;

  error:string;
  constructor(public dialog: MatDialog, public authenticationService: AuthenticationService, private tokenStorageService: TokenStorageService,
    public dialogRef: MatDialogRef<RegisterComponent>,
    private formBuilder: FormBuilder, public modal: NgbActiveModal,) {
    //this.user=data.user
  }
  get form() {
    return this.addUserForm?.controls;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    

    if (this.captchaResponse) {
      this.submitted = true;
      this.error=''
      let data = this.addUserForm.value
      let user = new User();
      user.email = data.email;
      user.password = data.password;
      user.tel = data.tel;
      user.firstName = data.firstName;
      user.lastName = '';
      user.acceptSms=data.acceptSms
      user.acceptEmail=data.acceptEmail

      if (this.addUserForm.status === 'VALID') {

      
        this.authenticationService.register(user).subscribe(
          result => {
            this.code = result.data.code
            if (this.code == 1) {
              Swal.fire({
                icon: 'error',
                title: 'Erreur Serveur',
                showConfirmButton: false,
                timer: 1500
              })
            }
            else if (this.code == 2) {
              this.emailUsed = data.email
            }
            else if (this.code == 3) {



   
              this.addUserForm.reset();
              this.submitted = false;
              this.tokenStorageService.saveToken(result.data.token)
              this.tokenStorageService.saveUser(result.data.user)
              window.location.reload();

            }
            else {

            }

           

          },
          error => {
            //error serveur
          } 
        );

      } else if (this.addUserForm.status === 'INVALID') {
        this.addUserForm.markAllAsTouched()
      }
    }
    else {

      this.error='ReCAPTCHA non valide. Veuillez réessayer.'
    }
  }

  createItem(data: any): FormGroup {
    return this.formBuilder.group(data);
  }
  /*
  openDialogLogin(): void {
  
    this.onNoClick()
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '250px',
      data: {user: this.user}
    });
 
    dialogRef.afterClosed().subscribe(result => {
      //this.user = result;
    });
  }*/
  public resolved(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
  ngOnInit(): void {
  }

}
