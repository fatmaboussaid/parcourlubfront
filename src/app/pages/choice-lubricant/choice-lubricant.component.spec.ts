import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceLubricantComponent } from './choice-lubricant.component';

describe('ChoiceLubricantComponent', () => {
  let component: ChoiceLubricantComponent;
  let fixture: ComponentFixture<ChoiceLubricantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoiceLubricantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiceLubricantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
