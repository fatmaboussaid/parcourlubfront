import { Component, OnInit } from '@angular/core';
import { Lubricant } from 'src/app/model/lubricant.model';
import { Station } from 'src/app/model/station.model';
import {Router, NavigationExtras,ActivatedRoute } from "@angular/router";
import { LubricantService } from 'src/app/service/lubricant.service';
import { StationService } from 'src/app/service/station.service';
import { Location } from '@angular/common'
import { CommandeService } from 'src/app/service/commande.service';
import { Commande } from 'src/app/model/commande.model';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-choice-lubricant',
  templateUrl: './choice-lubricant.component.html',
  styleUrls: ['./choice-lubricant.component.css']
})
export class ChoiceLubricantComponent implements OnInit {
  public parametre:Parametre = new Parametre()

  public lubricant: Lubricant;
  public stations: Station[]=[]
  public quantity:number=1;
  public id:number;
  datepipe: DatePipe = new DatePipe('en-US')
  public stationId: number
  constructor(private parametreServise:ParametreService,private activatedRoute: ActivatedRoute,public commandeService:CommandeService,public lubricantService:LubricantService,public stationService:StationService,private router: Router) { 
    this.parametre.devise= this.parametreServise.getDevise();
    this.activatedRoute.queryParams.subscribe(params => {
      this.id=params["id"];
      this.quantity=params["quantity"];
      this.lubricantService.getLubricant(this.id).subscribe((res) => {
         if(res['code'] == 1){           
           this.lubricant=res['product'];
           this.stations=this.lubricant.stations
          


          }
         else this.router.navigate(["/"]);

      });
    
    });
  }

  onImgError(event:any) { 
    event.target.src = 'assets/images/logo_60.png';
  }
  goToValidation1(stationId:number){
    let navigationExtras: NavigationExtras

    this.commandeService.saveCommandeStep11(this.lubricant.id,stationId,this.quantity).subscribe((res) => {
      if (res['code'] ==3) {
        navigationExtras = {
          queryParams: {
            "id": res['data'],
          }
        };

        this.router.navigate(["/validation1"], navigationExtras);
      }
      else {
        Swal.fire({
          icon: 'error',
          title: 'Erreur Serveur',
          showConfirmButton: false,
          timer: 1500
        })
      }


    });
    
  }
  ngOnInit(): void {
  }

}
