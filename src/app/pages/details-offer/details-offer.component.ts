import { Component, OnInit } from '@angular/core';
import {Router, NavigationExtras,ActivatedRoute } from "@angular/router";
import { Offer } from 'src/app/model/offer.model';
import { OfferService } from 'src/app/service/offer.service';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-details-offer',
  templateUrl: './details-offer.component.html',
  styleUrls: ['./details-offer.component.css']
})
export class DetailsOfferComponent implements OnInit {
  defaultLang:string|null='';
  public parametre:Parametre = new Parametre()
  public offer: Offer;
  public quantity:number=1;
  public id:number;
  constructor(public translateService: TranslateService,private parametreServise:ParametreService,private activatedRoute: ActivatedRoute,public offerService:OfferService, private router: Router) { 
    this.defaultLang=this.parametreServise.getLangue();
    console.log('2:'+this.defaultLang);

    this.parametre.devise= this.parametreServise.getDevise();
    this.activatedRoute.queryParams.subscribe(params => {
      this.id=params["id"];
      this.offerService.getOffer(this.id).subscribe((res) => {
         if(res['code'] == 1){
           this.offer=res['offer'];
         //  if(this.offer.quantity>0) this.quantity =1;
         //  else this.quantity =0;
          }
         else this.router.navigate(["/"]);

      });
    });
  }

  onClickMinus(){
    if(this.quantity>1) this.quantity --;
  }
  onClickPlus(){
   // if(this.quantity<this.lubricant.quantity) 
    this.quantity ++;
  }
  onImgError(event:any) { 
    event.target.src = 'assets/images/logo_60.png';
  }
  goToSelectStation(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id":this.offer.id,
        "quantity":this.quantity
      }
    };
    this.router.navigate(["/offer"],navigationExtras);
  }
  ngOnInit(): void {
  }

}
