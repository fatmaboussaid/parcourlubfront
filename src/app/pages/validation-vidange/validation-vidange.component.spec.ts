import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationVidangeComponent } from './validation-vidange.component';

describe('ValidationVidangeComponent', () => {
  let component: ValidationVidangeComponent;
  let fixture: ComponentFixture<ValidationVidangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationVidangeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationVidangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
