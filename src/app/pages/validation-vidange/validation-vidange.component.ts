import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common'
import { CommandeService } from 'src/app/service/commande.service';
import { Commande } from 'src/app/model/commande.model';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { User } from 'src/app/model/user.model';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-validation-vidange',
  templateUrl: './validation-vidange.component.html',
  styleUrls: ['./validation-vidange.component.css']
})
export class ValidationVidangeComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  token:string |null;
  userConnected:User;
  public commande: Commande = new Commande();
  datepipe: DatePipe = new DatePipe('en-US')
  paiement:string='1';
  cdtvente:boolean=false;
  constructor(private parametreServise:ParametreService,private tokenStorageService: TokenStorageService, public dialog: MatDialog,private router: Router, private location: Location, private activatedRoute: ActivatedRoute, public commandeService: CommandeService) {

    this.parametre.devise= this.parametreServise.getDevise();
   }
   onCheckboxChange(e: any) {

    if (e.target.checked) {
      this.cdtvente=e.target.value
    } else {
      this.cdtvente=false
    }
    
  }
   onImgError(event: any) {
    event.target.src = 'assets/images/logo_60.png';
  }
  openDialogLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '250px',
      data: { user: this.userConnected }
    });

    dialogRef.afterClosed().subscribe(result => {
      //this.user = result;
    });
  }
  submit(){
    
    this.commandeService.saveCommandeStep3(this.commande.id).subscribe((res) => {
      if (res['code'] == 3) {
      
      
        let navigationExtras: NavigationExtras
        navigationExtras = {
          queryParams: {
            "id":this.commande.id,
          }
        };
        this.router.navigate(["/thanks"], navigationExtras);
      }

      else {
        Swal.fire({
          icon: 'error',
          title: 'Erreur Serveur',
          showConfirmButton: false,
          timer: 1500
        })
      }


    });
    
  }

  back(): void {
    this.location.back()
  }

  goToStations(){
    let navigationExtras: NavigationExtras
    if(this.commande.offer)  {

      navigationExtras = {
        queryParams: {
          "id":this.commande.offer.id,
        }
      };
      this.router.navigate(["/offer"], navigationExtras);
    } 
    else if(this.commande.lubricant)   {
      navigationExtras = {
        queryParams: {
          "id":this.commande.lubricant.id,
          "quantity":this.commande.quantity
        }
      };
      this.router.navigate(["/lubricant1"], navigationExtras);
    }


 
  }

  goToVehicule(){
    let navigationExtras: NavigationExtras
    if(this.commande.offer)  {

      navigationExtras = {
        queryParams: {
          "id":this.commande.offer.id,
        }
      };
      this.router.navigate(["/offer"], navigationExtras);
    } 
    else if(this.commande.lubricant)   {
      navigationExtras = {
        queryParams: {
          "id":this.commande.lubricant.id,
          "quantity":this.commande.quantity
        }
      };
      this.router.navigate(["/lubricant1"], navigationExtras);
    }


 
  }
  goToSelectVehicule(){
    let navigationExtras: NavigationExtras

    if(this.commande.offer){
      navigationExtras = {
        queryParams: {
          "idOffer":this.commande.offer.id,
          "idStation":this.commande.station.id,
        }
      };
    }
    else if(this.commande.lubricant){
      navigationExtras = {
        queryParams: {
          "idLubricant":this.commande.lubricant.id,
          "idStation":this.commande.station.id,
        }
      };
    }
    else{
      navigationExtras = {
        queryParams: {
        }
      };
    }


    this.router.navigate(["/selectvehicule"], navigationExtras);
  }
  goToPrestations(){
    let navigationExtras: NavigationExtras={
      queryParams: {}
    } ;
    if(this.commande.lubricant){
     navigationExtras = {
      queryParams: {
        "marque":this.commande.vehicule.marque,
        "modele":this.commande.vehicule.modele,
        "year":this.commande.vehicule.year,
        "carburant":this.commande.vehicule.carburant,
        "idLubricant":this.commande.lubricant.id,
        "idStation":this.commande.station.id,
      }
    };
  }
  else if(this.commande.offer){
    navigationExtras = {
      queryParams: {
        "marque":this.commande.vehicule.marque,
        "modele":this.commande.vehicule.modele,
        "year":this.commande.vehicule.year,
        "carburant":this.commande.vehicule.carburant,
        "idOffer":this.commande.offer.id,
        "idStation":this.commande.station.id,
      }
    };
  }
    this.router.navigate(["/prestations"],navigationExtras);
  }




  ngOnInit(): void {


    this.token= this.tokenStorageService.getToken();
    this.userConnected= this.tokenStorageService.getUser();
    this.commande.prestations=[];
    this.activatedRoute.queryParams.subscribe(params => {

      if (params.id) {
        this.commandeService.getCommande(params.id).subscribe((res) => {
          this.commande = res
          if(this.commande.isCompleted){
            this.router.navigate(["/"]);
          }
           this.datepipe.transform(this.commande.dateOfReceipt, 'd MMM Y')
        
        });
      }
      else {
        this.router.navigate(["/"]);
      }

    });

  }

}