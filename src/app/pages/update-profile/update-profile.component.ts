import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/model/user.model';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { TokenStorageService } from 'src/app/service/token-storage.service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {
  submittedProfile: boolean = false;
  submittedPassword: boolean = false;

  code: number = 0;
  emailUsed: string;
  errorInfo: string;
  errorPassword: string;
  successInfo: string;
  successPassword: string;
  error: string;
  success: string;

  token: string | null;
  userConnected: User;
  updatePasswordForm = this.formBuilder.group(
    {
      oldpassword: ['', [Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}")]],
      newpassword: ['', [Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}")]],
      confirmpassword: ['', [Validators.required, 
        Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}"),]],

    }, {
      validator: this.MustMatch('newpassword', 'confirmpassword')
  });
  updateProfileForm = this.formBuilder.group(
    {
      firstName: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}")]],
      tel: ['', [Validators.required, Validators.pattern('^[0-9]{8}')]],
    }, {
    validator: []
  });
  constructor(private tokenStorageService: TokenStorageService, public authenticationService: AuthenticationService, private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) { }

  get formpassword() {
    return this.updatePasswordForm?.controls;
  }
  get formprofile() {
    return this.updateProfileForm?.controls;
  }
   MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
  }
  onSubmitPassword() {
    this.submittedPassword = true;
    this.errorPassword = ''
    this.successPassword = ''
    let data = this.updatePasswordForm.value
    
    if (this.updatePasswordForm.status === 'VALID') {
      this.authenticationService.updatePassword(data.oldpassword, data.newpassword).subscribe(
        result => {
          this.code = result.data.code
          if (this.code == 1) {
            //username is incorrect
            this.errorPassword = result.data.data
          }
          else if (this.code == 2) {
            //Le compte est désactivé.
            this.errorPassword = "Le compte est désactivé."
          }
          else if (this.code == 3) {
            //Le compte est désactivé.
            this.errorPassword = 'Votre ancien mot de passe est erroné'
          }
          else if (this.code == 4) {
            //Le compte est désactivé.
            this.successPassword = 'Votre mot de passe est modifier avec success'
          }
          else if (this.code == 5) {
            //Le compte est désactivé.
            this.errorPassword = 'Vérifier votre compte'
          }
          else if (this.code == 6) {
            //Le compte est désactivé.
            this.errorPassword ='Vérifier votre compte'
          }
          else if (this.code == 7) {
            //Le compte est désactivé.
            this.errorPassword = 'Vérifier votre compte'
          }
          else {

          }

          // 

          //message success
        },
        error => {
          //error serveur
        }
      );
    } else if (this.updatePasswordForm.status === 'INVALID') {
      this.updatePasswordForm.markAllAsTouched()
    }
  }
  onSubmitProfile() {
    this.submittedProfile = true;
    this.errorInfo = ''
    this.successInfo = ''
    let data = this.updateProfileForm.value
    if (this.updateProfileForm.status === 'VALID') {
      this.authenticationService.updateProfile(data.firstName, data.email, data.tel).subscribe(
        result => {
          this.code = result.data.code;
          if (this.code == 1) {
            //invalid token
            this.errorInfo = 'Email ou mot de passe incorrect'
          }
          else if (this.code == 2) {
            //email incorrect 
            this.errorInfo = 'Email ou mot de passe incorrect'

          }
          else if (this.code == 3) {
            //compte deactive 
            this.errorInfo = 'Email ou mot de passe incorrect'

          }
          else if (this.code == 4) {
            //incorrect password
            this.errorInfo = 'Email ou mot de passe incorrect'

          }
          else if (this.code == 5) {
            this.tokenStorageService.saveUser(result.data.user)
            window.location.reload();
            this.successInfo = ''

          }
          else if (this.code == 6) {
            this.errorInfo = 'Email déja existe'
          }
        },
        error => {
          //error serveur
        }
      );
    } else if (this.updatePasswordForm.status === 'INVALID') {
      this.updatePasswordForm.markAllAsTouched()
    }
  }
  ngOnInit(): void {

    this.token = this.tokenStorageService.getToken();
    this.userConnected = this.tokenStorageService.getUser();

    if (this.token) {


      this.authenticationService.refreshToken(this.token).subscribe(
        result => {
          this.code = result.data.code;
          if (this.code == 1) {
            //deactivated account
            this.error = 'Le compte est désactivé.'
          }
          else if (this.code == 2) {
            //user not client
            this.error = 'Vous devez créer un compte client'

          }
          else if (this.code == 3) {
            this.tokenStorageService.saveToken(result.data.token)
            this.tokenStorageService.saveUser(result.data.user)
            if ((this.userConnected.firstName != result.data.user.firstName)) {
              window.location.reload();
            }
            this.userConnected = this.tokenStorageService.getUser();

            this.updateProfileForm.controls.firstName.setValue(this.userConnected.lastName + ' ' + this.userConnected.firstName);
            this.updateProfileForm.controls.email.setValue(this.userConnected.email);
            this.updateProfileForm.controls.tel.setValue(this.userConnected.tel);


          }
          else if (this.code == 4) {
            //User Not Found
            this.error = 'Utilisateur introuvable'

          }
          else if (this.code == 5) {

            //Invalid Token
            this.error = 'Token invalide'
          }
          else if (this.code == 6) {
            //There is no token sent
            this.error = "Token n'existe pas"
          }
          else {
            this.error = 'Erreur serveur'
          }
        })
    }
    else {
      this.router.navigate(["/"]);
    }
  }

}
