import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common'
import { CommandeService } from 'src/app/service/commande.service';
import { Commande } from 'src/app/model/commande.model';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { User } from 'src/app/model/user.model';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-validation-lubricant',
  templateUrl: './validation-lubricant.component.html',
  styleUrls: ['./validation-lubricant.component.css']
})
export class ValidationLubricantComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  token:string |null;
  userConnected:User;
  public commande: Commande = new Commande();
  datepipe: DatePipe = new DatePipe('en-US')
  cdtvente:boolean=false;
  constructor(private parametreServise:ParametreService, private tokenStorageService: TokenStorageService, public dialog: MatDialog,private router: Router, private location: Location, private activatedRoute: ActivatedRoute, public commandeService: CommandeService) {
    this.parametre.devise= this.parametreServise.getDevise();
    this.activatedRoute.queryParams.subscribe(params => {

      if (params.id) {
        this.commandeService.getCommande(params.id).subscribe((res) => {
          this.commande = res
          if(this.commande.isCompleted){
            this.router.navigate(["/"]);
          }
           this.datepipe.transform(this.commande.dateOfReceipt, 'd MMM Y')
        
        });
      }
      else {
        this.router.navigate(["/"]);
      }

    });
  
  }
   onCheckboxChange(e: any) {

    if (e.target.checked) {
      this.cdtvente=e.target.value
    } else {
      this.cdtvente=false
    }
    
  }
   onImgError(event: any) {
    event.target.src = 'assets/images/logo_60.png';
  }
  openDialogLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '250px',
      data: { user: this.userConnected }
    });

    dialogRef.afterClosed().subscribe(result => {
      //this.user = result;
    });
  }
  submit(){
    this.commandeService.saveCommandeStep3(this.commande.id).subscribe((res) => {
      if (res['code'] == 3) {
      /*  Swal.fire({
          icon: 'success',
          title: 'Votre commande est en cours de traitement',
          showConfirmButton: false,
          timer: 1500
        })
        this.router.navigate(["/"]);
        */
        let navigationExtras: NavigationExtras
        navigationExtras = {
          queryParams: {
            "id":this.commande.id,
          }
        };
        this.router.navigate(["/thanks"], navigationExtras);
      }
      else {
        Swal.fire({
          icon: 'error',
          title: 'Erreur Serveur',
          showConfirmButton: false,
          timer: 1500
        })
      }


    });
  }
  
  back(): void {
    this.location.back()
  }
  goToProducts(){


    this.router.navigate(["/lubricants"]);
  }
  ngOnInit(): void {


    this.token= this.tokenStorageService.getToken();
    this.userConnected= this.tokenStorageService.getUser();
 
  }

}
