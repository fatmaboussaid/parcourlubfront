import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationLubricantComponent } from './validation-lubricant.component';

describe('ValidationLubricantComponent', () => {
  let component: ValidationLubricantComponent;
  let fixture: ComponentFixture<ValidationLubricantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidationLubricantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationLubricantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
