import { Component, ContentChild, ElementRef, OnInit, QueryList, ViewChild, ViewChildren, AfterContentInit, Renderer2 } from '@angular/core';
import { Commande } from 'src/app/model/commande.model';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { CommandeService } from 'src/app/service/commande.service';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { MatDialog } from '@angular/material/dialog';
import { DetailsCommandComponent } from 'src/app/modal/details-command/details-command.component';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-mes-commandes',
  templateUrl: './mes-commandes.component.html',
  styleUrls: ['./mes-commandes.component.css']
})
export class MesCommandesComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  p: number = 1;
  pr: number = 1;
  pc: number = 1;
  search: string ;

  public historiques: Commande[] = [];
  public rendezvous: Commande[] = [];
  public commandes: Commande[] = [];
  public historiquesFiltered: Commande[] =[]
   public maxheight=0
  constructor(private parametreServise:ParametreService,private renderer: Renderer2, public dialog: MatDialog, private router: Router, public commandeService: CommandeService, tokenService: TokenStorageService) {
    this.parametre.devise= this.parametreServise.getDevise();
    if (tokenService.getToken()) {
      this.getCommandes()
    }
    else {
      this.router.navigate(["/"]);
    }

  }
  getCommandes() {
    this.commandeService.getCommandesByUser().subscribe((res) => {
      this.historiques = res['historiques']
      this.historiquesFiltered = res['historiques']
      this.rendezvous = res['rendezvous']
      this.commandes = res['commandes']

    });

  }
  onImgError(event: any) {
    event.target.src = 'assets/images/logo_60.png';
  }
  updateHoraires(commande: Commande){
    console.log(commande);
    
    let navigationExtras: NavigationExtras
    navigationExtras = {
      queryParams: {
        "id":commande.id,
      }
    };

    this.router.navigate(["/updatehoraires"], navigationExtras);
  }
  annulerCommande(commande: Commande) {
    this.commandeService.annulerCommande(commande.id).subscribe((res) => {
      if (res.data.code == 3) {
        this.commandeService.getCommandesByUser().subscribe((res) => {
          this.historiques = res['historiques']
          this.historiquesFiltered = res['historiques']
          this.rendezvous = res['rendezvous']
          this.commandes = res['commandes']
          console.log(this.historiquesFiltered);
          
        });
      }
      else console.log(this.historiquesFiltered);
    });
  }

  openDialogDetails(commande: Commande): void {
    const dialogRef = this.dialog.open(DetailsCommandComponent, {
      width: '250px',
      data: {
        id: commande.id,
        vehicule: commande.vehicule,
        station: commande.station,
        offer: commande.offer,
        lubricant: commande.lubricant,
        prestations: commande.prestations,
        price: commande.price,
        dateOfReceipt: commande.dateOfReceipt,
        receptionPeriod: commande.receptionPeriod,
        createAt: commande.createAt,
        orderStatus: commande.orderStatus,
        isCompleted: commande.isCompleted,
        user: commande.user,
        uid: commande.uid,
        qrcode: commande.qrcode,
        quantity: commande.quantity
      }
    });

    dialogRef.afterClosed().subscribe(res => {

    });
  }
  onKeyupSearch(){
    console.log(this.search);
    this.search=this.search.toUpperCase();
    this.p=1
    this.historiquesFiltered=[]
    console.log( this.historiques);
    
    this.historiquesFiltered= this.historiques?.filter(commande => {
      if(commande.offer) {
        return commande.offer?.name.toUpperCase().includes(this.search) ;

      }
      else{
        return commande.lubricant?.name.toUpperCase().includes(this.search) ;

      }
      
   });
  }

  ngOnInit(): void {


  }
  ngForRendered(id:number) {
    console.log(id);
    

    
   
    this.commandes.forEach((commande, index) => {
    
      let div = document.getElementById('equalheight'+commande.id)
      var height:number =0
      if(div?.offsetHeight)height = div?.offsetHeight
      
      if( height > this.maxheight)this.maxheight=height

     
    });

    console.log(this.maxheight);


  }
  /*@ContentChild('Commandes', { static: false }) Commandes!: QueryList<ElementRef>;
  ngAfterViewInit()
  {
    console.log('aa');
     let maxHeight=0
     let height=0

     this.Commandes.forEach( x=>{
      height=x.nativeElement.getBoundingClientRect().height
       
       if (height>maxHeight)
          maxHeight=height
          console.log(maxHeight);

     })
  
  }*/
}
