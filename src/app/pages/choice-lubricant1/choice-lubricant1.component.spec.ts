import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceLubricant1Component } from './choice-lubricant1.component';

describe('ChoiceLubricant1Component', () => {
  let component: ChoiceLubricant1Component;
  let fixture: ComponentFixture<ChoiceLubricant1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoiceLubricant1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiceLubricant1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
