import { Component, OnInit } from '@angular/core';
import {Router, NavigationExtras,ActivatedRoute } from "@angular/router";
import { Lubricant } from 'src/app/model/lubricant.model';
import { Offer } from 'src/app/model/offer.model';
import { Station } from 'src/app/model/station.model';
import { LubricantService } from 'src/app/service/lubricant.service';
import { OfferService } from 'src/app/service/offer.service';
import { StationService } from 'src/app/service/station.service';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-choice-lubricant1',
  templateUrl: './choice-lubricant1.component.html',
  styleUrls: ['./choice-lubricant1.component.css']
})
export class ChoiceLubricant1Component implements OnInit {
  public parametre:Parametre = new Parametre()
  public lubricant: Lubricant=new Lubricant();
  public stations: Station[]=[]
  public quantity:number=0;
  public id:number;

  constructor(private parametreServise:ParametreService,private activatedRoute: ActivatedRoute,public lubricantService:LubricantService, public stationService:StationService,private router: Router) { 
    this.parametre.devise= this.parametreServise.getDevise();
    this.activatedRoute.queryParams.subscribe(params => {
      this.id=params["id"];
      this.quantity=params["quantity"];
      this.lubricantService.getLubricant(this.id).subscribe((res) => {
         if(res['code'] == 1){
           this.lubricant=res['product'];
           this.stations= this.lubricant.stations
          // if(this.offer.quantity==0) this.router.navigate(["/"]);
          }
         else this.router.navigate(["/"]);

      });
    
    });
  }


  goToSelectVehicule(stationId:number){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "idLubricant":this.lubricant.id,
        "idStation":stationId,
        "quantity":this.quantity,

      }
    };
    this.router.navigate(["/selectvehicule"],navigationExtras);
  }
  ngOnInit(): void {
  }


}
