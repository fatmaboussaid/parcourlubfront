import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common'
import { CommandeService } from 'src/app/service/commande.service';
import { Commande } from 'src/app/model/commande.model';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { User } from 'src/app/model/user.model';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.css']
})
export class ThanksComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  public commande: Commande = new Commande();
  datepipe: DatePipe = new DatePipe('en-US')

  constructor(private parametreServise:ParametreService,public tokenService: TokenStorageService, public dialog: MatDialog, private router: Router, private location: Location, private activatedRoute: ActivatedRoute, public commandeService: CommandeService) {
    this.parametre.devise= this.parametreServise.getDevise();
  }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.commande.vehicule.marque = ''
      this.activatedRoute.queryParams.subscribe(params => {

        if (params.id) {
          this.commandeService.getCommande(params.id).subscribe((res) => {
            this.commande = res
            if(!this.commande.isCompleted ){
              this.router.navigate(["/"]);
            }
            this.datepipe.transform(this.commande.dateOfReceipt, 'd MMM Y')

          });
        }
        else {
          this.router.navigate(["/"]);
        }

      });
    }
    else {
      this.router.navigate(["/"]);
    }


  }

}
