import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { Vehicule } from 'src/app/model/vehicule.model';
import { Station } from 'src/app/model/station.model';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { OfferService } from 'src/app/service/offer.service';
import { StationService } from 'src/app/service/station.service'
import { LubricantService } from 'src/app/service/lubricant.service';
import { RecommendedLubricant } from 'src/app/model/recommended-lubricant.model';
import { Lubricant } from 'src/app/model/lubricant.model';
import { Offer } from 'src/app/model/offer.model';
import {listCarburants} from "src/app/service/const"
import { SearchService } from 'src/app/service/search.service';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-choice-station',
  templateUrl: './choice-station.component.html',
  styleUrls: ['./choice-station.component.css']
})
export class ChoiceStationComponent implements OnInit {
  public parametre:Parametre = new Parametre()
  public vehicule: Vehicule = new Vehicule();
  public station: Station = new Station();
  public recommendedLubricants: Number[] = []
  public alternativeLubricants: Number[] = []
  public resultList: any
  public search: number;

  constructor( private parametreServise:ParametreService,public searchService: SearchService,private location: Location, private activatedRoute: ActivatedRoute, public lubricantService: LubricantService, public stationService: StationService, public offerService: OfferService, private router: Router) {
    this.parametre.devise= this.parametreServise.getDevise();
    this.activatedRoute.queryParams.subscribe(params => {
      this.vehicule.marque = params.marque;
      this.vehicule.modele = params.modele;
      this.vehicule.year = params.year;
      this.vehicule.carburant = params.carburant;
      this.station.id = params.id;
      this.search = params.search;

    });
    this.stationService.getStationById(this.station.id).subscribe((res: Station) => {
      this.station = res
    });
    this.lubricantService.getLubricantRecommended(this.vehicule.marque, this.vehicule.modele, this.vehicule.year).subscribe((res: any) => {
      this.recommendedLubricants = res.recommendedLubricants
      this.alternativeLubricants = res.alternativeLubricants

      let carburantType:number=0;
      listCarburants.filter( (c:string,i:number)  =>
        {
          
          if(this.vehicule.carburant==c){
            carburantType=i;
          }
    
        }
        );
      this.lubricantService.getRecommendedAndAlternativeLubricants(carburantType,this.recommendedLubricants, this.alternativeLubricants, this.station.id).subscribe((res: any) => {
        this.resultList =res

      });
    });

  }

  onImgError(event:any) { 
    event.target.src = 'assets/images/logo_60.png';
  }
  back(): void {
    this.location.back()
  }

  goToPrestationLubricant(lubricant:Lubricant){
    
    this.searchService.steps(this.search,2,'Produit').subscribe(
      result => {
       
      });
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "marque":this.vehicule.marque,
        "modele":this.vehicule.modele,
        "year":this.vehicule.year,
        "carburant":this.vehicule.carburant,
        "idLubricant":lubricant.id,
        "idStation":this.station.id,
        "search": this.search,

      }
    };
    this.router.navigate(["/prestations"],navigationExtras);

  }
  goToPrestationOffer(offer:Offer){
    this.searchService.steps(this.search,2,'Offre').subscribe(
      result => {
      });
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "marque":this.vehicule.marque,
        "modele":this.vehicule.modele,
        "year":this.vehicule.year,
        "carburant":this.vehicule.carburant,
        "idOffer":offer.id,
        "idStation":this.station.id,
        "search": this.search,

      }
    };
    this.router.navigate(["/prestations"],navigationExtras);
  }
  ngOnInit(): void {
  }

}
