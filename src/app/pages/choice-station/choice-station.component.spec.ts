import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceStationComponent } from './choice-station.component';

describe('ChoiceStationComponent', () => {
  let component: ChoiceStationComponent;
  let fixture: ComponentFixture<ChoiceStationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoiceStationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiceStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
