import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceOfferComponent } from './choice-offer.component';

describe('ChoiceOfferComponent', () => {
  let component: ChoiceOfferComponent;
  let fixture: ComponentFixture<ChoiceOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoiceOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiceOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
