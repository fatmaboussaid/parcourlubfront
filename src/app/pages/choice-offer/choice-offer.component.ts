import { Component, OnInit } from '@angular/core';
import {Router, NavigationExtras,ActivatedRoute } from "@angular/router";
import { Offer } from 'src/app/model/offer.model';
import { Station } from 'src/app/model/station.model';
import { OfferService } from 'src/app/service/offer.service';
import { StationService } from 'src/app/service/station.service';
@Component({
  selector: 'app-choice-offer',
  templateUrl: './choice-offer.component.html',
  styleUrls: ['./choice-offer.component.css']
})
export class ChoiceOfferComponent implements OnInit {
  public offer: Offer=new Offer();
  public stations: Station[]=[]
  public quantity:number=1;
  public id:number;

  constructor(private activatedRoute: ActivatedRoute,public offerService:OfferService, public stationService:StationService,private router: Router) { 
    this.activatedRoute.queryParams.subscribe(params => {
      this.id=params["id"];
      this.quantity=params["quantity"];
      this.offerService.getOffer(this.id).subscribe((res) => {
         if(res['code'] == 1){
           this.offer=res['offer'];
           this.stations= this.offer.stations
          // if(this.offer.quantity==0) this.router.navigate(["/"]);
          }
         else this.router.navigate(["/"]);

      });
    
    });
  }


  goToSelectVehicule(stationId:number){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "idOffer":this.offer.id,
        "idStation":stationId,
        "quantity":this.quantity,

      }
    };
    this.router.navigate(["/selectvehicule"],navigationExtras);
  }
  ngOnInit(): void {
  }

}
