import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { LubricantComponent } from './pages/lubricant/lubricant.component';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule} from '@angular/material/toolbar';
import {NgxPaginationModule} from 'ngx-pagination';
import { OfferComponent } from './pages/offer/offer.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {  MatInputModule } from '@angular/material/input';
import { DetailsLubricantComponent } from './pages/details-lubricant/details-lubricant.component';
import { DetailsOfferComponent } from './pages/details-offer/details-offer.component';
import { ChoiceOfferComponent } from './pages/choice-offer/choice-offer.component';
import { ItemOfferComponent } from './layout/item-offer/item-offer.component';
import { ItemStationComponent } from './layout/item-station/item-station.component';
import { ChoiceLubricantComponent } from './pages/choice-lubricant/choice-lubricant.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import {NgbModule,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MatDialogModule} from '@angular/material/dialog';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ChoiceVehiculeComponent } from './pages/choice-vehicule/choice-vehicule.component';
import { VehiculeItemComponent } from './layout/vehicule-item/vehicule-item.component';
import { ChoiceStationComponent } from './pages/choice-station/choice-station.component';
import { ItemLubricantComponent } from './layout/item-lubricant/item-lubricant.component';
import { ListPrestationsComponent } from './pages/list-prestations/list-prestations.component';
import { HorairesComponent } from './pages/horaires/horaires.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import { ValidationComponent } from './pages/validation/validation.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { SelectVehiculeComponent } from './pages/select-vehicule/select-vehicule.component';
import { RouterModule, ROUTES } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ValidationLubricantComponent } from './pages/validation-lubricant/validation-lubricant.component';
import { ThanksComponent } from './pages/thanks/thanks.component';
import { MesCommandesComponent } from './pages/mes-commandes/mes-commandes.component'
import { LOCALE_ID } from '@angular/core';
import { ItemStationRadioComponent } from './layout/item-station-radio/item-station-radio.component';
import { DetailsCommandComponent } from './modal/details-command/details-command.component';
import { HorairesOfferComponent } from './pages/horaires-offer/horaires-offer.component';
import { ValidationVidangeComponent } from './pages/validation-vidange/validation-vidange.component';
import { ChoiceLubricant1Component } from './pages/choice-lubricant1/choice-lubricant1.component';
import { FooterComponent } from './layout/footer/footer.component';
import { RecaptchaModule } from 'ng-recaptcha';
import { RestPasswordComponent } from './modal/rest-password/rest-password.component';
import { UpdatePasswordComponent } from './pages/update-password/update-password.component';
import { UpdateProfileComponent } from './pages/update-profile/update-profile.component';
import { NotfoundComponent } from './layout/notfound/notfound.component';
import { EventEmitterService } from './service/event-emitter.service';
import { AgmCoreModule } from '@agm/core';

import { HttpClient } from '@angular/common/http';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { UpdateHorairesComponent } from './pages/update-horaires/update-horaires.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LubricantComponent,
    OfferComponent,
    AccueilComponent,
    DetailsLubricantComponent,
    DetailsOfferComponent,
    ChoiceOfferComponent,
    ItemOfferComponent,
    ItemStationComponent,
    ChoiceLubricantComponent,
    LoginComponent,
    RegisterComponent,
    ChoiceVehiculeComponent,
    VehiculeItemComponent,
    ChoiceStationComponent,
    ItemLubricantComponent,
    ListPrestationsComponent,
    HorairesComponent,
    ValidationComponent,
    SelectVehiculeComponent,
    ValidationLubricantComponent,
    ThanksComponent,
    MesCommandesComponent,
    ItemStationRadioComponent,
    DetailsCommandComponent,
    HorairesOfferComponent,
    ValidationVidangeComponent,
    ChoiceLubricant1Component,
    FooterComponent,
    RestPasswordComponent,
    UpdatePasswordComponent,
    UpdateProfileComponent,
    NotfoundComponent,
    UpdateHorairesComponent,


  ],
  imports: [
   // RouterModule.forRoot(routes:Routes, { useHash: true }) ,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    NgxPaginationModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    NgbModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule,
    MatRadioModule,
    RecaptchaModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDYhfYs2iY5PFut6nyg40SkRzC4iiJk98A'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    NgxPaginationModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    NgbModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule,
    MatRadioModule

  ],
  providers: [
    EventEmitterService,
     { provide: LOCALE_ID, useValue: "fr-FR" },
     DatePipe,
     {provide: LocationStrategy,useClass: HashLocationStrategy},NgbActiveModal],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
export function translateFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}
