import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/model/user.model';
import { LoginComponent } from 'src/app/pages/login/login.component';
import { RegisterComponent } from 'src/app/pages/register/register.component';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { Output, EventEmitter } from '@angular/core';
import { EventEmitterService } from 'src/app/service/event-emitter.service';
import { ParametreService } from 'src/app/service/parametre.service';
import { Paramtype } from '@nestjs/common';
import { Parametre } from 'src/app/model/parametre.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user: User;
  token: string | null;
  userConnected: User;
  search: string;
  inputvalue: string;
  langue: string | null = this.parametreService.getLangue();
  langues: string | null = this.parametreService.getLangues();
  visibleSearch: string = ''
  constructor(private parametreService: ParametreService, private eventEmitterService: EventEmitterService, private tokenStorageService: TokenStorageService, public dialog: MatDialog, private translateService: TranslateService) {
    this.parametreService.getParametre().subscribe((res: Parametre) => {
      if (res.langues) {
        this.langues = JSON.stringify(res.langues)
        //get first item in langues 
       
        if (parametreService.getLangue()) this.langue = parametreService.getLangue();
        else {
          if (res.langues.filter(x => typeof x !== undefined).shift()?.toString() !== undefined) {
            this.langue = res.langues.filter(x => typeof x !== undefined).shift() + '';
          }
        }
        if (parametreService.getLangues()) this.langues = parametreService.getLangues();
      };
    });

  

  }
  onKeyupSearch() {
    this.eventEmitterService.sendSearch(this.search);
  }
  blockSearch(visibleSearch: string) {
    this.visibleSearch = visibleSearch;
  }
  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();

  }
  openDialogRegister(): void {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '250px',
      data: { user: this.user }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.user = result;
    });
  }
  openDialogLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '250px',
      data: { user: this.user }
    });

    dialogRef.afterClosed().subscribe(result => {
      //this.user = result;
    });
  }
  ngOnInit(): void {
    this.token = this.tokenStorageService.getToken();
    this.userConnected = this.tokenStorageService.getUser();
  }

  translateSite(language: string) {
    this.langue = language
    this.translateService.use(language);
    this.translateService.setDefaultLang(language);
    this.parametreService.saveLangue(language);
    console.log('1:' + language);

     window.location.reload();
  }

}
