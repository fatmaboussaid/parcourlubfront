import { Component, Input, OnInit } from '@angular/core';
import { Station } from 'src/app/model/station.model';
import { Vehicule } from 'src/app/model/vehicule.model';
import {Router, NavigationExtras,ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-item-station-radio',
  templateUrl: './item-station-radio.component.html',
  styleUrls: ['./item-station-radio.component.css']
})
export class ItemStationRadioComponent implements OnInit {
  @Input() station :Station;
  @Input() vehicule :Vehicule;

  constructor(private router: Router) { }
  goToStation(stationId:number){

    let navigationExtras: NavigationExtras = {
      queryParams: {
        "marque":this.vehicule.marque,
        "modele":this.vehicule.modele,
        "year":this.vehicule.year,
        "carburant":this.vehicule.carburant,
        "id":stationId,
      }
    };
    this.router.navigate(["/station"],navigationExtras);
   }
  ngOnInit(): void {
  }

}
