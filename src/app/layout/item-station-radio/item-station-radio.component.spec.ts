import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemStationRadioComponent } from './item-station-radio.component';

describe('ItemStationRadioComponent', () => {
  let component: ItemStationRadioComponent;
  let fixture: ComponentFixture<ItemStationRadioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemStationRadioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemStationRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
