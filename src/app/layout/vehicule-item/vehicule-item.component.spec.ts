import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiculeItemComponent } from './vehicule-item.component';

describe('VehiculeItemComponent', () => {
  let component: VehiculeItemComponent;
  let fixture: ComponentFixture<VehiculeItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehiculeItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiculeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
