import { Component, OnInit,Input  } from '@angular/core';
import { Vehicule } from 'src/app/model/vehicule.model';

@Component({
  selector: 'app-vehicule-item',
  templateUrl: './vehicule-item.component.html',
  styleUrls: ['./vehicule-item.component.css']
})
export class VehiculeItemComponent implements OnInit {
  @Input() vehicule :Vehicule;

  constructor() { }

  ngOnInit(): void {
    
  }

}
