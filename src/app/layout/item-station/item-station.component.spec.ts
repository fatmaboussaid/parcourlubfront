import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemStationComponent } from './item-station.component';

describe('ItemStationComponent', () => {
  let component: ItemStationComponent;
  let fixture: ComponentFixture<ItemStationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemStationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
