import { Component, OnInit,Input } from '@angular/core';
import { Station } from 'src/app/model/station.model';

@Component({
  selector: 'app-item-station',
  templateUrl: './item-station.component.html',
  styleUrls: ['./item-station.component.css']
})
export class ItemStationComponent implements OnInit {
  @Input() station :Station;
  constructor() { }

  ngOnInit(): void {
  }

}
