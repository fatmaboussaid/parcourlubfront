import { Component, OnInit,Input } from '@angular/core';
import { Lubricant } from 'src/app/model/lubricant.model';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';

@Component({
  selector: 'app-item-lubricant',
  templateUrl: './item-lubricant.component.html',
  styleUrls: ['./item-lubricant.component.css']
})
export class ItemLubricantComponent implements OnInit {
  @Input() lubricant :Lubricant;
  public parametre:Parametre = new Parametre()
  constructor(private parametreServise:ParametreService) {
    this.parametre.devise= this.parametreServise.getDevise();
   }

  ngOnInit(): void {
  }

}
