import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemLubricantComponent } from './item-lubricant.component';

describe('ItemLubricantComponent', () => {
  let component: ItemLubricantComponent;
  let fixture: ComponentFixture<ItemLubricantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemLubricantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemLubricantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
