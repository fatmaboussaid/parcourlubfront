import { Component, OnInit,Input } from '@angular/core';
import { Offer } from 'src/app/model/offer.model';
import { Parametre } from 'src/app/model/parametre.model';
import { ParametreService } from 'src/app/service/parametre.service';
@Component({
  selector: 'app-item-offer',
  templateUrl: './item-offer.component.html',
  styleUrls: ['./item-offer.component.css']
})
export class ItemOfferComponent implements OnInit {
  @Input() offer :Offer;
  public parametre:Parametre = new Parametre()
  constructor(private parametreServise:ParametreService) {
    this.parametre.devise= this.parametreServise.getDevise();
   }
  onImgError(event:any) { 
    event.target.src = 'assets/images/logo_60.png';
  }
  ngOnInit(): void {
  }

}
