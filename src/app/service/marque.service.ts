import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {BASE_URL_LUBRICANT} from "./const"
import { Marque } from '../model/marque.model';
@Injectable({
  providedIn: 'root'
})
export class MarqueService {

  apiURLLUBRICANT = BASE_URL_LUBRICANT;
  constructor(private http: HttpClient) { }
  getMarque() {
    return this.http.get<Marque[]>(this.apiURLLUBRICANT+'searchMarqueModelYearByCountry?country_code=TN').pipe();;
  }
}
