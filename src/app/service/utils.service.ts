import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators'
import {BASE_URL} from "./const"
import { Station } from '../model/station.model';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  public watchId: number;
  public locationTrackingActive = false;
  public currentLocation: { latitude: number, longitude: number } ;
  private reactiveDeviceLocation$: Subject<Object>;
  constructor(private http: HttpClient) { 
    this.reactiveDeviceLocation$ = new BehaviorSubject<Object>(this.currentLocation);

  }
  getAddress(laltitude:number,longitude:number):Observable<any> {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+laltitude+','+longitude+'&sensor=true&key=AIzaSyDYhfYs2iY5PFut6nyg40SkRzC4iiJk98A').pipe();
  }

  

getLocation1(): { latitude: number, longitude: number } {
  const opts = { enableHighAccuracy: true, maximumAge: 60000, timeout: 30000 };
  this.watchId = navigator.geolocation.watchPosition((position) => {
      this.locationTrackingActive = true;
      this.currentLocation = { latitude: position.coords.latitude, longitude: position.coords.longitude };      
      this.reactiveDeviceLocation$.next(this.currentLocation);
  },
  (err) => {
      this.locationTrackingActive = false;
  },
  opts);
  return this.currentLocation;


}

    /*getLocation():number[] {
  if (navigator.geolocation) {
      var location_timeout = setTimeout("geolocFail()", 10000);
      navigator.geolocation.getCurrentPosition(position=> {
        clearTimeout(location_timeout);
        if (position) {
          return [ position.coords.latitude, position.coords.longitude]
        }
      },
        function (error) {
          return [];
        }, {
        enableHighAccuracy: true
        , timeout: 5000
      }
      );
     
    } else {
       return [];
    }
    return [];
   
  } */

  getTypeDevice():number {
    let typedevice = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(typedevice)) {
      //device= "tablet";
     return  1;
   }
   else if (/Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(typedevice  )
   ) {
      //device= "mobile";
      return  2;
   }
   else{
      //device= "desktop";
      return  3;
   }
  }
  loadIp() {

    this.http.get('https://jsonip.com').subscribe(
      (value:any) => {
        console.log(value);
        let ipadress = value.ip;
        console.log(ipadress);
        let url = `http://api.ipstack.com/${value.ip}?access_key=Your_API_Key`
        let address= this.http.get(url);
        console.log(address);
      },
      (error) => {
        console.log(error);
      }
    );
      
  }
}
