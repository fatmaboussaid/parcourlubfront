import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {BASE_URL} from "./const"
import { User } from '../model/user.model';
import { TokenStorageService } from './token-storage.service';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient,private tokenStorageService:TokenStorageService) { }
  apiURL = BASE_URL;
  token:any =this.tokenStorageService.getToken();
  httpOptionsWithoutToken = {
    'headers':{'Content-Type':  'application/json',}
  };
    
  httpOptionsWithToken = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + this.token
    })
  };
  register(user:User): Observable<any> {
    const data = new FormData();
    data.append('email',  user.email);
    data.append('tel', user.tel);
    data.append('firstName', user.firstName);
    data.append('lastName',  user.lastName);
    data.append('password',  user.password);
    data.append('acceptEmail',  user.acceptEmail+'');
    data.append('acceptSms',  user.acceptSms+'');

    data.append('content-type', 'application/json');

    return this.http.post(this.apiURL+'authentication/inscription', data);
  }
  generateToken(email:string,password:string): Observable<any> {
    const data = new FormData();
    data.append('email',  email);
    data.append('password',  password);
    data.append('content-type', 'application/json');
    return this.http.post(this.apiURL+'authentication/GeneRateT0ken', data);
  }
  login(token:string): Observable<any> {
    const data = new FormData();
    data.append('Authorization',  token);
    data.append('content-type', 'application/json');
    return this.http.post(this.apiURL+'authentication/login', data);
  }
  refreshToken(token:string): Observable<any> {
    const data = new FormData();
    data.append('Authorization',  token);
    data.append('content-type', 'application/json');
    return this.http.post(this.apiURL+'authentication/RefreshT0ken', data);
  }
  forgetPassword(email:string): Observable<any> {
    const data = new FormData();
    data.append('email',  email);
    data.append('content-type', 'application/json');
    return this.http.post(this.apiURL+'authentication/forget_password', data);
  }
  resetPassword(token:string,password:string,confirmpassword:string): Observable<any> {
    const data = new FormData();
    data.append('token',  token);
    data.append('password',  password);
    data.append('confirmpassword',  confirmpassword);

    data.append('content-type', 'application/json');
    return this.http.post(this.apiURL+'authentication/reset_password', data);
  }
  updateProfile(firstName:string,email:string,tel:string): Observable<any> {
    const data = new FormData();
    data.append('Authorization',  this.token);
    data.append('email',  email);
    data.append('tel', tel);
    data.append('firstName',firstName);
    data.append('content-type', 'application/json');
    return this.http.post(this.apiURL+'authentication/updateProfile', data);
  }
  updatePassword(oldpassword:string,newpassword:string): Observable<any> {
    const data = new FormData();
    data.append('Authorization',  this.token);
    data.append('oldpassword',  oldpassword);
    data.append('newpassword', newpassword);
    data.append('content-type', 'application/json');
    return this.http.post(this.apiURL+'authentication/updatePassword', data);
  }
}
