import { Injectable } from '@angular/core';
import { Parametre } from '../model/parametre.model';
import {BASE_URL} from "./const"
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators'
import { TranslateService } from '@ngx-translate/core';
const DEVISE_KEY = 'devise';
const DEVISE_FR_KEY = 'deviseFr';
const DEVISE_EN_KEY = 'deviseEn';
const DEVISE_AR_KEY = 'deviseAr';
const LANGUE_KEY='defaultLang'
const LANGUES_KEY='Langs'
@Injectable({
  providedIn: 'root'
})
export class ParametreService {
  apiURL = BASE_URL;
  langue:string|null='en';
  constructor(private http: HttpClient,private translateService:TranslateService) { } 
  public saveDevise(deviseFr: string,deviseEn: string,deviseAr: string): void {
    try {
      window.localStorage.removeItem(DEVISE_FR_KEY);
      window.localStorage.setItem(DEVISE_FR_KEY, deviseFr);    
      window.localStorage.removeItem(DEVISE_EN_KEY);
      window.localStorage.setItem(DEVISE_EN_KEY, deviseEn);   
      window.localStorage.removeItem(DEVISE_AR_KEY);
      window.localStorage.setItem(DEVISE_AR_KEY, deviseAr);     
    } catch (err) {
      return err
    }

  }
  public getDevise(): string | null {
    try {
      this.langue=this.getLangue();
     if(this.langue == 'fr') return window.localStorage.getItem(DEVISE_FR_KEY);
     else if (this.langue == 'en') return window.localStorage.getItem(DEVISE_EN_KEY);
     else if(this.langue == 'ar') return window.localStorage.getItem(DEVISE_AR_KEY);
     else return '';

     
    } catch (err) {
      return err
    }

  }
  public saveLangue(langue: string): void {
    try {
      window.localStorage.removeItem(LANGUE_KEY);
      window.localStorage.setItem(LANGUE_KEY, langue);    
     
    } catch (err) {
      return err
    }

  }
  public getLangue(): string | null {
    try {
      return window.localStorage.getItem(LANGUE_KEY);
    } catch (err) {
      return err
    }

  }
  public saveLangues(langues: string): void {
    
    try {
      window.localStorage.removeItem(LANGUES_KEY);
      window.localStorage.setItem(LANGUES_KEY, langues);    
     
    } catch (err) {
      return err
    }

  }
  public getLangues(): string | null {
    try {
      return window.localStorage.getItem(LANGUES_KEY);
    } catch (err) {
      return err
    }

  }
  getParametre():Observable<Parametre> {
    return this.http.get<Parametre>(this.apiURL+'parametre').pipe();;
  }
}
