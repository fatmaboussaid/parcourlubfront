import { EventEmitter, Injectable } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';    

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {

  search = new EventEmitter();    
  subsVar: Subscription;    
    
  constructor() { }    
    
  sendSearch(search:string) {    
    this.search.emit(search);    
  }    
}
