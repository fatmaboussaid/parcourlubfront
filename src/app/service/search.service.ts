import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {BASE_URL} from "./const"
import { Lubricant } from '../model/lubricant.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  apiURL = BASE_URL;
  constructor(private http: HttpClient) { }
  saveSearch(form:any,address:string) {
    const data = new FormData();
    data.append('marque',  form.marque);
    data.append('modele', form.modele);
    data.append('year', form.year);
    data.append('carburant', form.carburant);
    data.append('address',address);
    data.append('content-type', 'application/json');    
    return this.http.post(this.apiURL+'search/addSearch', data);
  }
  steps(id:number,stepNumber:number,step:string) {
    const data = new FormData();
    data.append('id', id+'');
    data.append('step',step);
    data.append('stepNumber',stepNumber+'');
    data.append('content-type', 'application/json');    
    return this.http.post(this.apiURL+'search/steps', data);
  }

}
