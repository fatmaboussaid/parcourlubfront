import { Injectable } from '@angular/core';
import { User } from '../model/user.model';
import {  CookieService  } from 'ngx-cookie-service';
const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const Cookies_KEY = 'cookies-accepted';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor(public cookieService:CookieService) { }
  signOut(): void {
    window.sessionStorage.clear();
  }

  public saveToken(token: string): void {
    try {
      window.sessionStorage.removeItem(TOKEN_KEY);
      window.sessionStorage.setItem(TOKEN_KEY, token);
    } catch (err) {
      return err
    }

  }

  public getToken(): string | null {
    try {
      return window.sessionStorage.getItem(TOKEN_KEY);
    } catch (err) {
      return err
    }

  }
  public saveCookies(): void {
    try {
      console.log('aaaaa');

      window.localStorage.setItem(Cookies_KEY, '1');
    } catch (err) {
      return err
    }

  }

  public getCookies(): string | null {
    try {
      return window.localStorage.getItem(Cookies_KEY);
    } catch (err) {
      return err
    }

  }

  public saveUser(user: User): void {
    try {
      window.sessionStorage.removeItem(USER_KEY);
      window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    } catch (err) {
      return err
    }

  }

  public getUser(): User {
    try {
      const user = window.sessionStorage.getItem(USER_KEY);


      if (user) {
        return JSON.parse(user);
      }
      return new User();
    } catch (err) {
      return new User();
    }

  }
}
