import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {BASE_URL} from "./const"
import { User } from '../model/user.model';
import { TokenStorageService } from './token-storage.service';
import { Commande } from '../model/commande.model';
import { UtilsService } from 'src/app/service/utils.service';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {
  constructor(private http: HttpClient,private tokenStorageService:TokenStorageService, public utilsService: UtilsService) { }
  apiURL = BASE_URL;
  token:any =this.tokenStorageService.getToken();
  user:User =this.tokenStorageService.getUser();
  device:number = this.utilsService.getTypeDevice();

  httpOptionsWithoutToken = {
    'headers':{'Content-Type':  'application/json',}
  };
    
  httpOptionsWithToken = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': this.token
    })
  };
  saveCommandeStep1(commande:Commande,prestationsIds:Number[]): Observable<any> {
    const data = new FormData();
    data.append('marque',  commande.vehicule.marque);
    data.append('modele',  commande.vehicule.modele);
    data.append('year',  commande.vehicule.year);
    console.log(this.device);
    data.append('carburant',  commande.vehicule.carburant);    
    if(this.device==2)    data.append('typeDevice',  "mobile");
    else data.append('typeDevice',  "desktop");
    if(commande.station)data.append('stationId',  commande.station.id+'');
    if(commande.offer){
      data.append('offerId',  commande.offer.id+'');
      if(commande.quantity) data.append('quantity',  commande.quantity+'');
      
    }

    if(commande.lubricant){
      data.append('productId',  commande.lubricant.id+'');
     if(commande.quantity) data.append('quantity',  commande.quantity+'');
    }
    data.append('prestationsIds', JSON.stringify (prestationsIds));
    if(this.user) data.append('userId', this.user.id+'');

    data.append('content-type', 'application/json');

    return this.http.post(this.apiURL+'commande/saveCommandeStep1', data);
  }
  saveCommandeStep11(productId:number,stationId:number,quantity:number): Observable<any> {
    console.log(this.device);
    
    const data = new FormData();
    data.append('productId',  productId+'');
    data.append('stationId',  stationId+'');
    data.append('quantity',  quantity+'');
    if(this.device==2)    data.append('typeDevice',  "mobile");
    else data.append('typeDevice',  "desktop");
    if(this.user) data.append('userId', this.user.id+'');

    data.append('content-type', 'application/json');

    return this.http.post(this.apiURL+'commande/saveCommandeStep11', data);
  }
  saveCommandeStep2(id:number,date:string,hour:string): Observable<any>{
    const data = new FormData();
    data.append('date', date);
    data.append('hour',  hour);
    data.append('id',  id+'');
    if(this.user) data.append('userId', this.user.id+'');
    return this.http.post(this.apiURL+'commande/saveCommandeStep2', data);

  }

  saveCommandeStep3(id:number,): Observable<any>{
    const data = new FormData();
    data.append('userId', this.user.id+'');
    data.append('id',  id+'');
    return this.http.post(this.apiURL+'commande/saveCommandeStep3', data);

  }


  getCommande(id:number): Observable<any> {
    return this.http.get(this.apiURL+'commande/getCommande?id='+id).pipe();
  }
  getCommandesByDate(date:any): Observable<Commande[]> {
    return this.http.get<Commande[]>(this.apiURL+'commande/getCommandesByDate?date='+date).pipe();
  }
  getCommandesByUser(): Observable<any> {
    return this.http.get<Commande[]>(this.apiURL+'commande/getCommandesByUser?user='+this.user.id).pipe();
  }  
  annulerCommande(id:number): Observable<any>{
    const data = new FormData();
    data.append('Authorization',  this.token);
    data.append('id',  id+'');
    return this.http.post(this.apiURL+'commande/annulerCommande', data);

  }
  updateDateCommande(id:number,date:string,hour:string): Observable<any>{
    const data = new FormData();
    data.append('Authorization',  this.token);
    data.append('date', date);
    data.append('hour',  hour);
    data.append('id',  id+'');
    return this.http.post(this.apiURL+'commande/updateDateCommande', data);

  }
}
