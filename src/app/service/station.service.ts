import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators'
import {BASE_URL} from "./const"
import { Station } from '../model/station.model';

@Injectable({
  providedIn: 'root'
})
export class StationService {

  apiURL = BASE_URL;
  constructor(private http: HttpClient) { }
  getStations(isOilChange=false,latitude:number=0,longitude:number=0) {
    let params=''
    if(latitude!=0 && longitude!=0) params='&latitude='+latitude+'&longitude='+longitude
    return this.http.get<Station[]>(this.apiURL+'station/listStations?isOilChange='+isOilChange+params).pipe();;
  }

  getStationById(id:number):Observable<Station> {
    return this.http.get<Station>(this.apiURL+'station/getStation?id='+id).pipe();;
  }
}
