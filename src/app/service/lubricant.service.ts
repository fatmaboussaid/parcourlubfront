import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {BASE_URL,BASE_URL_LUBRICANT} from "./const"
import { Lubricant } from '../model/lubricant.model';
import { RecommendedLubricant } from '../model/recommended-lubricant.model';

@Injectable({
  providedIn: 'root'
})
export class LubricantService {
  apiURL = BASE_URL;
  apiURLLUBRICANT = BASE_URL_LUBRICANT;
  constructor(private http: HttpClient) { }
  getLubricants(max=0) {
    return this.http.get<Lubricant[]>(this.apiURL+'product/listProducts?max='+max).pipe();
  }
  getLubricant(id:number): Observable<any> {
    return this.http.get(this.apiURL+'product/getProduct?id='+id).pipe();
  }
  getRecommendedAndAlternativeLubricants(carburantType:Number,recommendedLubricants:Number[],alternativeLubricants:Number[],stationid:Number) {
    return this.http.get<Lubricant[]>(this.apiURL+'product/getRecommendedProducts?stationid='+stationid+
    '&recommendedLubricants='+JSON.stringify(recommendedLubricants)+
    '&alternativeLubricants='+JSON.stringify(alternativeLubricants)+
    '&carburantType='+carburantType).pipe();
  }
  getLubricantRecommended(marque:string,modele:string,year:string): any {
    return this.http.get(this.apiURLLUBRICANT+'getLubricantRecommended?marque='+marque+'&modele='+modele+'&year='+year+'&country_code=TN').pipe();
  }
}
