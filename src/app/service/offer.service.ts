import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {BASE_URL} from "./const"
import { Offer } from '../model/offer.model';
@Injectable({
  providedIn: 'root'
})
export class OfferService {

  apiURL = BASE_URL;
  constructor(private http: HttpClient) { }
  getOffers(max=0) {
    return this.http.get<Offer[]>(this.apiURL+'offer/listOffers?max='+max).pipe();;
  }
  getOffer(id:number): Observable<any> {
    return this.http.get(this.apiURL+'offer/getOffer?id='+id).pipe();
  }
}
