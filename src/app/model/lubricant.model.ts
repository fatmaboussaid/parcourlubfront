import { Offer } from "./offer.model";
import { Station } from "./station.model";

export class Lubricant {
    public id: number;
    public name: string;
    public description: string;
    public price: number;
    public pricePromo: number;
    public isPromo: boolean;
    public image: string;
    public file: string;
    public filePath: string;
    public quantity:number;
    public offers:Offer[];
    public stations:Station[]=[];

    constructor() {

    }
    getName(): string {
      return `${this.name} ${this.name}`;
    }
}
