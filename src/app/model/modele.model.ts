import { Marque } from "./marque.model";

export class Modele {
    public id: number;
    public marque: Marque;
    public modele: string;
    public annee_debut: string;
    public annee_fin: string;

    constructor() {

    }
}
