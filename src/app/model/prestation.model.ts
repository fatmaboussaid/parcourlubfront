import { PrestationStation } from "./prestation-station.model";
import { Station } from "./station.model";

export class Prestation {
    public id: number;
    public icon: string;
    public name: string;
    public nameEn: string;
    public nameAr: string;

    public PrestationStations: PrestationStation[];

 

    constructor() {
    }
}
