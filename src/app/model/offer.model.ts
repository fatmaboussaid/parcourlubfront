import { PrestationStation } from "./prestation-station.model";
import { Prestation } from "./prestation.model";
import { Station } from "./station.model";

export class Offer {
    public id: number;
    public name: string;
    public nameEn: string;
    public nameAr: string;
    public description: string;
    public descriptionEn: string;
    public descriptionAr: string;
    public price: number;
    public pricePromo: number;
    public isPromo: boolean;
    public image: string;
    public imageEn: string;
    public imageAr: string;
    public quantity:number;
    public file: string;
    public filePath: string;
    public fileEn: string;
    public filePathEn: string;
    public fileAr: string;
    public filePathAr: string;
    public stations:Station[]=[];
    public prestations: Prestation[]=[];

    constructor() {

    }
}
