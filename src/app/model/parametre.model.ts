import { ParametreService } from "../service/parametre.service";

export class Parametre {
    public devise: string |null;
    public deviseFr: string |null;
    public deviseEn: string |null;
    public deviseAr: string |null;
    public langues: string[] ;
    constructor() {
    
    }
    getDevise(parametreServise:ParametreService): string|null {
        return parametreServise.getDevise() ;
      }

}
