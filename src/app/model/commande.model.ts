import { Lubricant } from "./lubricant.model";
import { Offer } from "./offer.model";
import { Prestation } from "./prestation.model";
import { Station } from "./station.model";
import { User } from "./user.model";
import { Vehicule } from "./vehicule.model";

export class Commande {
    public id: number;
    public vehicule: Vehicule;
    public station: Station;
    public offer: Offer;
    public lubricant: Lubricant;
    public prestations:Prestation[]=[];
    public price: number;
    public dateOfReceipt: Date;
    public receptionPeriod: string;
    public createAt:Date
    public orderStatus: number;
    public isCompleted: boolean;
    public user: User;
    public uid: number;
    public qrcode: number;
    public quantity: number;

    constructor() {
        this.station=new Station();
        this.lubricant=new Lubricant();
        this.offer=new Offer();
        this.user=new User();
        this.vehicule=new Vehicule();
        this.prestations=[];

    }
}
