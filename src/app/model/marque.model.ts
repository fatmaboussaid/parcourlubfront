import { Modele } from "./modele.model";

export class Marque {
    public id: number;
    public marque: string;
    public modeles:Modele[]=[];
    constructor() {

    }
}
