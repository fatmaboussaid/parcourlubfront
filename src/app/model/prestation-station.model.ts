import { Prestation } from "./prestation.model";
import { Station } from "./station.model";

export class PrestationStation {
    public id:number;
    public station:Station;
    public prestation: Prestation;
}
