export class Vehicule {
    public marque: string;
    public modele: string;
    public year: string;
    public carburant: string;

    constructor() {
        this.marque='';
    }
}
