import { PrestationStation } from "./prestation-station.model";
import { Prestation } from "./prestation.model";

export class Station {
    public id: number;
    public name: string;
    public address: string;
    public tel: string;
    public prestationStations: PrestationStation[];
    public horaires: [];
    public isClosedSunday: boolean;
    public isOilChange: boolean;
    public image: string;
    public file: string;
    public filePath: string;
    public horaireValues = [
        { 1: "de 8h à 10h" },
        { 2: "de 10h à 12h" },
        { 3: "de 12h à 14h" },
        { 4: "de 14h à 16h" },
        { 5: "de 16h à 18h" },
        { 6: "de 18h à 20h" },
    ];

    constructor() {
    }






}
