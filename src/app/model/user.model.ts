export class User {
    public id: number;
    public email: string;
    public password: string;
    public tel: string;
    public firstName: string;
    public lastName: string;
    public acceptSms: boolean;
    public acceptEmail: boolean;

    constructor() {
    }

}
